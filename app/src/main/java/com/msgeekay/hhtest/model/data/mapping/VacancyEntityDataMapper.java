package com.msgeekay.hhtest.model.data.mapping;

import com.msgeekay.hhtest.model.Vacancy;
import com.msgeekay.hhtest.model.data.ListWrapper;
import com.msgeekay.hhtest.model.data.VacancyEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class VacancyEntityDataMapper
{
  public static List<Vacancy> transform(ListWrapper<VacancyEntity> wrapper)
  {
    List<VacancyEntity> vacancyEntityList = new ArrayList<>();
    vacancyEntityList.addAll(wrapper.items);

    return transform(vacancyEntityList);
  }

  public static List<Vacancy> transform(Collection<VacancyEntity> vacancyEntityCollection)
  {
    List<Vacancy> vacancyList = new ArrayList<>();

    if (vacancyEntityCollection != null)
    {
      for (VacancyEntity entity : vacancyEntityCollection)
      {
        Vacancy n = transform(entity);
        if (n != null)
          vacancyList.add(n);
      }
    }

    return vacancyList;
  }

  public static Vacancy transform(VacancyEntity vacancyEntity)
  {
    Vacancy retVal = null;

    if (vacancyEntity != null)
    {
      String url = vacancyEntity.getImageURI();
      if (url == null || (url != null && url.length() == 0))
        url = vacancyEntity.getImageURIFromEmployer();
      retVal = new Vacancy(vacancyEntity.getId(), vacancyEntity.getDescription(), vacancyEntity.getName(),
              url);
    }

    return retVal;
  }

  public static VacancyEntity transform(Vacancy vacancy)
  {
    VacancyEntity retVal = null;

    if (vacancy != null)
      retVal = new VacancyEntity(vacancy.getId(), vacancy.getDescription(), vacancy.getName(),
              vacancy.getImageURI());

    return retVal;
  }
}
