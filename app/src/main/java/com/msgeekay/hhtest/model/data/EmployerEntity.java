package com.msgeekay.hhtest.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class EmployerEntity
{
  @SerializedName("logo_urls")
  @Expose
  public UrlEntity urlEntity;
}
