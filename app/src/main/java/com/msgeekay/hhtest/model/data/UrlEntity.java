package com.msgeekay.hhtest.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class UrlEntity
{
  @SerializedName("original")
  @Expose
  public String url;
}
