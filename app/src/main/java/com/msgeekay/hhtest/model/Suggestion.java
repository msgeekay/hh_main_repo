package com.msgeekay.hhtest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class Suggestion
{
  @SerializedName("text")
  @Expose
  private String text;

  public Suggestion(String text)
  {
    this.text = text;
  }

  public String getText()
  {
    return text;
  }

  public void setText(String text)
  {
    this.text = text;
  }


}
