package com.msgeekay.hhtest.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class SuggestionEntity
{
  public SuggestionEntity(String text)
  {
    this.text = text;
  }

  @SerializedName("text")
  @Expose
  private String text;

  public String getText()
  {
    return text;
  }

  public void setText(String text)
  {
    this.text = text;
  }
}
