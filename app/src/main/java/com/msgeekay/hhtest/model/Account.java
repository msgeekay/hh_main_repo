package com.msgeekay.hhtest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class Account
{
  @SerializedName("id")
  @Expose
  private long id;

  @SerializedName("login")
  @Expose
  private String login;

  @SerializedName("email")
  @Expose
  private String email;

  @SerializedName("firstName")
  @Expose
  private String firstName;

  @SerializedName("lastName")
  @Expose
  private String lastName;

  @SerializedName("lang")
  @Expose
  private String lang;

  @SerializedName("region")
  @Expose
  private String region;
}
