package com.msgeekay.hhtest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
`* Account's authorization (not in use)
 * Created by grigoriykatz on 03/10/17.
 */

public class AccountAuth
{
  @SerializedName("account")
  @Expose
  private Account account;

  @SerializedName("accessToken")
  @Expose
  private String accessToken;

  public Account getAccount() {
    return account;
  }

  public void setAccount(Account account) {
    this.account = account;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  @Override
  public String toString() {
    return "AccountAuth{" +
            "account=" + account +
            ", accessToken='" + accessToken + '\'' +
            '}';
  }
}
