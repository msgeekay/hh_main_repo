package com.msgeekay.hhtest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class Vacancy
{
  private long id;
  private String description;
  private String name;
  private String imageURI;

  public Vacancy(long id, String description, String name, String imageURI)
  {
    this.id = id;
    this.description = description;
    this.name = name;
    this.imageURI = imageURI;
  }


  public String getImageURI()
  {
    return imageURI;
  }

  public void setImageURI(String imageURI)
  {
    this.imageURI = imageURI;
  }



  public void setId(long id)
  {
    this.id = id;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public void setName(String name)
  {
    this.name = name;
  }



  public long getId()
  {
    return id;
  }

  public String getDescription()
  {
    return description;
  }

  public String getName()
  {
    return name;
  }
}
