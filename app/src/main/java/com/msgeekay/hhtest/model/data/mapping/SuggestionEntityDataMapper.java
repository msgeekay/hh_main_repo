package com.msgeekay.hhtest.model.data.mapping;

import com.msgeekay.hhtest.model.Suggestion;
import com.msgeekay.hhtest.model.Vacancy;
import com.msgeekay.hhtest.model.data.ListWrapper;
import com.msgeekay.hhtest.model.data.SuggestionEntity;
import com.msgeekay.hhtest.model.data.VacancyEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class SuggestionEntityDataMapper
{
  public static List<Suggestion> transform(ListWrapper<SuggestionEntity> wrapper)
  {
    List<SuggestionEntity> suggestionEntityList = new ArrayList<>();
    suggestionEntityList.addAll(wrapper.items);

    return transform(suggestionEntityList);
  }

  public static List<Suggestion> transform(Collection<SuggestionEntity> suggestionEntityCollection)
  {
    List<Suggestion> suggestionList = new ArrayList<>();

    if (suggestionEntityCollection != null)
    {
      for (SuggestionEntity entity : suggestionEntityCollection)
      {
        Suggestion n = transform(entity);
        if (n != null)
          suggestionList.add(n);
      }
    }

    return suggestionList;
  }

  public static Suggestion transform(SuggestionEntity suggestionEntity)
  {
    Suggestion retVal = null;

    if (suggestionEntity != null)
      retVal = new Suggestion(suggestionEntity.getText());

    return retVal;
  }

  public static SuggestionEntity transform(Suggestion suggestion)
  {
    SuggestionEntity retVal = null;

    if (suggestion != null)
      retVal = new SuggestionEntity(suggestion.getText());

    return retVal;
  }
}
