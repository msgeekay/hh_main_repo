package com.msgeekay.hhtest.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.msgeekay.hhtest.datalayer.db.VacsDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by grigoriykatz on 03/10/17.
 */

@Table(database = VacsDatabase.class)
public class VacancyEntity extends BaseModel
{
  @PrimaryKey
  @Column
  @SerializedName("id")
  @Expose
  private long id;

  @Column
  @SerializedName("description")
  @Expose
  private String description;

  @Column
  @SerializedName("name")
  @Expose
  private String name;

  @Column
  private String imageURI;

  @SerializedName("employer")
  @Expose
  private EmployerEntity employerEntity;

  @Column
  public long page;
  @Column
  public long per_page;
  @Column
  public long timestamp;

  public VacancyEntity() {}

  public VacancyEntity(long id, String description, String name, String imageURI)
  {
    this.id = id;
    this.description = description;
    this.name = name;
    this.imageURI = imageURI;
  }

  public long getId()
  {
    return id;
  }

  public void setId(long id)
  {
    this.id = id;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getImageURI()
  {
    return imageURI;
  }

  public void setImageURI(String imageURI)
  {
    this.imageURI = imageURI;
  }

  public String getImageURIFromEmployer()
  {
    if (employerEntity == null)
      return null;
    if (employerEntity.urlEntity == null)
      return null;
    if (employerEntity.urlEntity.url == null)
      return null;
    return employerEntity.urlEntity.url;
  }
}
