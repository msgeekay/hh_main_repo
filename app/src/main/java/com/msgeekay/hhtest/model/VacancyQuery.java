package com.msgeekay.hhtest.model;

import com.msgeekay.hhtest.presentationlayer.MyApp;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class VacancyQuery
{
  public VacancyQuery() {
    page = Integer.valueOf(0);
    per_page = Integer.valueOf(20);
    text = "";
    AccountAuth aauth = MyApp.getInstance().getAccountAuth(false);
    account =  aauth == null ? null : aauth.getAccount();
  }

  private Account account;
  private Integer page;
  private Integer per_page;
  private String text;

  public Account getAccount()
  {
    return account;
  }

  public void setAccount(Account account)
  {
    this.account = account;
  }

  public Integer getPage()
  {
    return page;
  }

  public void setPage(Integer page)
  {
    this.page = page;
  }

  public Integer getPer_page()
  {
    return per_page;
  }

  public void setPer_page(Integer per_page)
  {
    this.per_page = per_page;
  }

  public String getText()
  {
    return text;
  }

  public void setText(String text)
  {
    this.text = text;
  }





}
