package com.msgeekay.hhtest.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class ListWrapper<T> {

  @SerializedName("items")
  @Expose
  public List<T> items;
}
