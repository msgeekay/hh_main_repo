package com.msgeekay.hhtest.domainlayer.interactor;

import com.msgeekay.hhtest.domainlayer.executor.PostExecutionThread;
import com.msgeekay.hhtest.domainlayer.executor.ThreadExecutor;
import com.msgeekay.hhtest.domainlayer.repo.Repository;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class GetVacancy extends UseCase<Long>
{
  private Repository repo;

  @Inject
  public GetVacancy(Repository repo, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread)
  {
    super(threadExecutor, postExecutionThread);
    this.repo = repo;
  }
  @Override
  protected Observable buildUseCaseObservable(Long vacancyId)
  {
    return repo.vacancy(vacancyId);
  }
}
