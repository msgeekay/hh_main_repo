package com.msgeekay.hhtest.domainlayer.repo;

import com.msgeekay.hhtest.model.Suggestion;
import com.msgeekay.hhtest.model.Vacancy;

import java.util.List;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public interface Repository
{
  Observable<List<Vacancy>> vacancies(String text, Integer page, Integer per_page);
  Observable<Vacancy> vacancy(final Long vacancyId);

  Observable<List<Suggestion>> suggestions(String text);


}
