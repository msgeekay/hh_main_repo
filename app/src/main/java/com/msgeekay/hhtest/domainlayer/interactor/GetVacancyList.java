package com.msgeekay.hhtest.domainlayer.interactor;

import com.msgeekay.hhtest.domainlayer.executor.PostExecutionThread;
import com.msgeekay.hhtest.domainlayer.executor.ThreadExecutor;
import com.msgeekay.hhtest.domainlayer.repo.Repository;
import com.msgeekay.hhtest.model.VacancyQuery;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class GetVacancyList extends UseCase<VacancyQuery>
{
  private Repository repo;

  @Inject
  public GetVacancyList(Repository repo, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread)
  {
    super(threadExecutor, postExecutionThread);
    this.repo = repo;
  }
  @Override
  protected Observable buildUseCaseObservable(VacancyQuery vacancyQuery)
  {
    return repo.vacancies(vacancyQuery.getText(), vacancyQuery.getPage(), vacancyQuery.getPer_page());
  }
}
