package com.msgeekay.hhtest.datalayer.image.implv2;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.msgeekay.hhtest.datalayer.image.ImageLoader;

import java.lang.ref.WeakReference;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by grigoriykatz on 04/10/17.
 */

@Singleton
public class ImageLoaderImpl implements ImageLoader
{
  private Context mContext;

  @Inject
  public ImageLoaderImpl(Context context)
  {
    this.mContext = context;
  }

  @Override
  public void load(WeakReference<ImageView> imageViewWR, String url)
  {
    ImageView view = imageViewWR.get();
    if (view != null)
      Glide.with(mContext).load(url).into(view);
  }
}
