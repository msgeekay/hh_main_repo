package com.msgeekay.hhtest.datalayer.repo.datastore;

import android.net.Uri;

import com.msgeekay.hhtest.datalayer.db.DBApi;
import com.msgeekay.hhtest.model.data.ListWrapper;
import com.msgeekay.hhtest.model.data.SuggestionEntity;
import com.msgeekay.hhtest.model.data.VacancyEntity;

import java.util.List;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by grigoriykatz on 04/10/17.
 */

public class VacanciesDBDataStore implements DataStore
{
  private DBApi dbApi;

  public VacanciesDBDataStore(DBApi dbApi)
  {
    this.dbApi = dbApi;
  }

  @Override
  public Observable<List<VacancyEntity>> vacancies(String text, Integer page, Integer per_page)
  {
    return dbApi.vacancies(text, page, per_page);
  }

  @Override
  public Observable<VacancyEntity> vacancy(Long vacancyId)
  {
    return dbApi.vacancy(vacancyId);
  }

  @Override
  public Observable<Response<ListWrapper<SuggestionEntity>>> suggestions(String text)
  {
    return Observable.error(new Exception("offline"));
  }

  @Override
  public Observable<Uri> dataExport(String containerName, String data)
  {
    throw new UnsupportedOperationException("Operation is not available!!!");
  }
}
