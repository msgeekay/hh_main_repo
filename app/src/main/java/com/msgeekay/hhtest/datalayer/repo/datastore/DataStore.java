package com.msgeekay.hhtest.datalayer.repo.datastore;

import android.net.Uri;

import com.msgeekay.hhtest.model.Suggestion;
import com.msgeekay.hhtest.model.data.ListWrapper;
import com.msgeekay.hhtest.model.data.SuggestionEntity;
import com.msgeekay.hhtest.model.data.VacancyEntity;

import java.util.List;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public interface DataStore
{
  //Observable<ListWrapper<VacancyEntity>>  vacancies(String text, Integer page, Integer per_page);
  //Observable<Response<ListWrapper<VacancyEntity>>>  vacancies(String text, Integer page, Integer per_page);
  Observable<List<VacancyEntity>> vacancies(String text, Integer page, Integer per_page);

  //Observable<Response<VacancyEntity>> vacancy(final Long vacancyId);
  Observable<VacancyEntity> vacancy(final Long vacancyId);

  Observable<Response<ListWrapper<SuggestionEntity>>> suggestions(String text);

  Observable<Uri> dataExport(String containerName, String data);
}
