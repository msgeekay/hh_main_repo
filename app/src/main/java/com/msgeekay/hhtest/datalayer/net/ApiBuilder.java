package com.msgeekay.hhtest.datalayer.net;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.msgeekay.hhtest.BuildConfig;
import com.msgeekay.hhtest.model.AccountAuth;
import com.msgeekay.hhtest.presentationlayer.MyApp;
import com.msgeekay.hhtest.presentationlayer.utils.Parameters;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by grigoriykatz on 03/10/17.
 */
@Singleton
public final class ApiBuilder
{
  @Inject
  public Retrofit getRetrofit(String url)
  {
    OkHttpClient okHttpClient = createHttpClient();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .client(okHttpClient)
            .build();

    return retrofit;
  }

  /**
   * Получить интерфейс Апи
   * @param retrofit
   * @return интерфейс апи
   */
  @Inject
  public BaseApi getApiInterface(Retrofit retrofit)
  {
    return retrofit.create(BaseApi.class);
  }

  /**
   * Создаем HttpClient
   * @return okHttpClient
   */
  private static OkHttpClient createHttpClient() {

    OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
    httpClientBuilder.addInterceptor(chain -> {
      Request original = chain.request();
      Request.Builder requestBuilder = original.newBuilder()
              .header("User-Agent", "hh_android")
//                    .header("User-Agent", String.format("%s.version.%s", App.getInstance().getPackageName(), BuildConfig.VERSION_CODE))
              .method(original.method(), original.body());

      AccountAuth accountAuth = MyApp.getInstance().getAccountAuth(false);
      if (accountAuth != null) {
        requestBuilder.header("Authorization", "token " + accountAuth.getAccessToken());
      }
      Request request = requestBuilder.build();
      return chain.proceed(request);
    });

    if (Parameters.DEBUG)
    {
      httpClientBuilder.addNetworkInterceptor(new StethoInterceptor());
    }

    if (BuildConfig.DEBUG) {
      HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
      logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
      httpClientBuilder.addInterceptor(logging);
    }
    return httpClientBuilder.build();
  }
}
