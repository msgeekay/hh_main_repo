package com.msgeekay.hhtest.datalayer.image.implv1;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.msgeekay.hhtest.R;
import com.msgeekay.hhtest.datalayer.image.ImageLoader;
import com.msgeekay.hhtest.presentationlayer.MyApp;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import timber.log.Timber;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class ImageLoaderImpl implements ImageLoader
{
  private static ImgCache imgCache;
  private static DiskImgCache diskImgCache;
  private final Set<Target> protectedFromGarbageCollectorTargets;

  private BlockingQueue<Runnable> imgQueue;
  private final ThreadFactory threadFactory;
  private final ThreadPoolExecutor threadPoolExecutor;

  public ImageLoaderImpl()
  {
    this.imgQueue = new LinkedBlockingDeque<>();
    this.threadFactory = new ImageThreadFactory();
    this.threadPoolExecutor = new ThreadPoolExecutor(2, 4,
              20, TimeUnit.SECONDS, imgQueue, threadFactory);


    this.imgCache = ImgCache.getInstance();
    this.diskImgCache = DiskImgCache.getInstance();
    this.protectedFromGarbageCollectorTargets = new HashSet<>();

  }

  public void load(WeakReference<ImageView> imageViewWR, String url)
  {

    this.threadPoolExecutor.execute(provideNextRunable(imageViewWR, url));


  }

  public Runnable provideNextRunable(WeakReference<ImageView> imageViewWR, String url)
  {
    return new Runnable()
    {
      @Override
      public void run()
      {
        if (url == null || url.isEmpty()) {
          return;
        }

        if (imgCache.getLru().get(url) != null && imageViewWR.get() != null)
        {
          imageViewWR.get().post(new Runnable()
          {
            @Override
            public void run()
            {
              imageViewWR.get().setImageBitmap(imgCache.getLru().get(url));
            }
          });

        }
        else if (diskImgCache.containsKey(url) && imageViewWR.get() != null)
        {
          diskImgCache.getImageFromCache(imageViewWR.get(), url);
        }
        else
        {
          final Target tg = new Target()
          {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
            {
              if (bitmap != null)
              {
                if (imageViewWR.get() != null)
                {
                  imageViewWR.get().post(new Runnable()
                  {
                    @Override
                    public void run()
                    {
                      imageViewWR.get().setImageBitmap(bitmap);
                    }
                  });

                }
                diskImgCache.addImageToCache(url, bitmap);
              }
//          if (protectedFromGarbageCollectorTargets.contains(tg))
//            protectedFromGarbageCollectorTargets.remove(tg);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable)
            {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable)
            {

            }
          };

          protectedFromGarbageCollectorTargets.add(tg);

          if (imageViewWR.get() != null)
          {
            imageViewWR.get().post(new Runnable()
            {
              @Override
              public void run()
              {
                loadImageIntoTarget(tg, url);
              }
            });
          }

        }
      }
    };

    //loadCustomSize(imageView, imageView.getLayoutParams().width, imageView.getLayoutParams().height, url);
  }

  public static void loadImageIntoTarget(Target target, String url) {
    Timber.v(url);
    Picasso.with(MyApp.getInstance()).load(url).placeholder(R.drawable.default_profile_background)
            .error(R.drawable.default_profile_background).into(target);
    //ImageLoaderNew.getInstance().load(url, target);
  }

  public static void loadCustomSize(ImageView imageView, int width, int height, String url) {
    if (url == null || url.isEmpty()) {
      return;
    }
    loadImage(imageView, url);
  }

  private static void loadImage(ImageView imageView, String url) {
    if (url == null || url.isEmpty()) {
      return;
    }
    Timber.v(url);
    Picasso.with(MyApp.getInstance()).load(url).into(imageView);
    //ImageLoaderNew.getInstance().load(url, imageView);
  }


  private static class ImageThreadFactory implements ThreadFactory {
    private static final String THREAD_NAME = "android_img_";
    private int counter = 0;

    @Override public Thread newThread(Runnable runnable) {
      return new Thread(runnable, THREAD_NAME + counter++);
    }
  }
}
