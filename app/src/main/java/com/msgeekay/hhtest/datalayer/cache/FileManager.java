package com.msgeekay.hhtest.datalayer.cache;

import android.net.Uri;
import android.os.Environment;
import android.support.annotation.WorkerThread;
import android.widget.Toast;

import com.msgeekay.hhtest.presentationlayer.MyApp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by grigoriykatz on 19/09/17.
 *
 * Class for I/O operations w/ files/dirs
 */

@Singleton
public class FileManager
{
  public static final String APPLICATION_NAME = "hhtest";

  @Inject
  public FileManager() {}



  public Uri writeData(String containerName, String fileContent)
  {
    File f = checkAndPrepareFile(containerName);
    if (f == null)
      return null;

    return writeToFile(f, fileContent);

  }


  public File checkAndPrepareFile(String containerName)
  {
    if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
    {
      Toast.makeText(MyApp.getInstance(), "no SD card", Toast.LENGTH_LONG).show();
      return null;
    }
    else
    {
      String dir = getHomeFolder();
      File file = new File(dir, containerName + ".txt");

      try
      {
        file.createNewFile();
      }
      catch (IOException e)
      {
        Toast.makeText(MyApp.getInstance(), "Error on file creation", Toast.LENGTH_LONG).show();
        return null;
      }

      return file;
    }
  }

  /**
   * Writes a file to Disk.
   * This is an I/O operation and this method executes in the main thread, so it is recommended to
   * perform this operation using another thread.
   *
   * @param file The file to write to Disk.
   */
  @WorkerThread
  public Uri writeToFile(File file, String fileContent)
  {
    try
    {
      FileWriter writer = new FileWriter(file);
      writer.write(fileContent);
      writer.close();

      return Uri.fromFile(file);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {

    }

    return null;

  }

  /**
   * Reads a content from a file.
   * This is an I/O operation and this method executes in the main thread, so it is recommended to
   * perform the operation using another thread.
   *
   * @param file The file to read from.
   * @return A string with the content of the file.
   */
  public String readFileContent(File file) {
    StringBuilder fileContentBuilder = new StringBuilder();
    if (file.exists()) {
      String stringLine;
      try {
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        while ((stringLine = bufferedReader.readLine()) != null) {
          fileContentBuilder.append(stringLine + "\n");
        }
        bufferedReader.close();
        fileReader.close();
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    return fileContentBuilder.toString();
  }

  /**
   * Returns a boolean indicating whether this file can be found on the underlying file system.
   *
   * @param file The file to check existence.
   * @return true if this file exists, false otherwise.
   */
  public boolean exists(File file) {
    return file.exists();
  }

  /**
   * Warning: Deletes the content of a directory.
   * This is an I/O operation and this method executes in the main thread, so it is recommended to
   * perform the operation using another thread.
   *
   * @param directory The directory which its content will be deleted.
   */
  public void clearDirectory(File directory) {
    if (directory.exists()) {
      for (File file : directory.listFiles()) {
        file.delete();
      }
    }
  }

  public static String getHomeFolder()
  {
    String dir = Environment.getExternalStorageDirectory() + File.separator + APPLICATION_NAME;
    File folder = new File(dir); //folder name
    folder.mkdirs();
    return dir;
  }

  public File buildFile(String containerName) {
    StringBuilder fileNameBuilder = new StringBuilder();
    fileNameBuilder.append(getHomeFolder());
    fileNameBuilder.append(File.separator);
    fileNameBuilder.append(containerName);
    fileNameBuilder.append(".txt");

    return new File(fileNameBuilder.toString());
  }
}
