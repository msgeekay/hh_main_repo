package com.msgeekay.hhtest.datalayer.image;

import android.widget.ImageView;

import java.lang.ref.WeakReference;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public interface ImageLoader
{
  void load(WeakReference<ImageView> imageViewWR, String url);
}
