package com.msgeekay.hhtest.datalayer.db;

import com.msgeekay.hhtest.model.data.VacancyEntity;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.Database;
import com.raizlabs.android.dbflow.sql.language.Delete;

/**
 * Created by grigoriykatz on 04/10/17.
 */

@Database(name = VacsDatabase.NAME, version = VacsDatabase.VERSION,
        insertConflict = ConflictAction.IGNORE, updateConflict= ConflictAction.REPLACE)
public class VacsDatabase
{
  static final String NAME = "vacs_db";

  static final int VERSION = 1;

  public static void clear() {
    Delete.tables(VacancyEntity.class);

    //TODO implement
  }
}
