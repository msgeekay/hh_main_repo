package com.msgeekay.hhtest.datalayer.net;

import com.msgeekay.hhtest.model.Suggestion;
import com.msgeekay.hhtest.model.data.ListWrapper;
import com.msgeekay.hhtest.model.data.SuggestionEntity;
import com.msgeekay.hhtest.model.data.VacancyEntity;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by grigoriykatz on 14/05/17.
 */

public interface BaseApi
{
  @GET("vacancies")
  Observable<Response<ListWrapper<VacancyEntity>>> getVacancies(
          @Query("text") String searchText,
          @Query("page") Integer page,
          @Query("per_page") Integer per_page);

//  @GET("vacancies")
//  Observable<Response<ResponseBody>> getVacancies(
//          @Query("text") String searchText,
//          @Query("page") Integer page,
//          @Query("per_page") Integer per_page);

  @GET("vacancies/{vacancyId}")
  Observable<Response<VacancyEntity>> getVacancyById(
          @Path("vacancyId") Long vacancyId);

  @GET("/suggests/vacancy_search_keyword")
  Observable<Response<ListWrapper<SuggestionEntity>>> getSearchSuggestions(
          @Query("text") String searchText);

}
