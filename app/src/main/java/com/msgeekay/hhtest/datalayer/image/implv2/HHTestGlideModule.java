package com.msgeekay.hhtest.datalayer.image.implv2;

import android.content.Context;

import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by grigoriykatz on 04/10/17.
 */

@GlideModule
public class HHTestGlideModule extends AppGlideModule
{
  @Override
  public void applyOptions(Context context, GlideBuilder builder) {
    int memoryCacheSizeBytes = 1024 * 1024 * 10; // 20mb
    builder.setMemoryCache(new LruResourceCache(memoryCacheSizeBytes));

    int diskCacheSizeBytes = 1024 * 1024 * 100; // 100 MB
    builder.setDiskCache(new InternalCacheDiskCacheFactory(context, diskCacheSizeBytes));
  }

  @Override
  public boolean isManifestParsingEnabled() {
    return false;
  }
}
