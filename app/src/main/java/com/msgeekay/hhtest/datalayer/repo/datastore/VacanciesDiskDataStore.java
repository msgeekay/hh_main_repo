package com.msgeekay.hhtest.datalayer.repo.datastore;

import android.net.Uri;

import com.msgeekay.hhtest.datalayer.cache.DataCache;
import com.msgeekay.hhtest.model.data.ListWrapper;
import com.msgeekay.hhtest.model.data.SuggestionEntity;
import com.msgeekay.hhtest.model.data.VacancyEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public class VacanciesDiskDataStore implements DataStore
{
  private DataCache dataCache;

  public VacanciesDiskDataStore(DataCache dataCache)
  {
    this.dataCache = dataCache;
  }


  @Override
  //public Observable<Response<ListWrapper<VacancyEntity>>> vacancies(String text, Integer page, Integer per_page)
  public Observable<List<VacancyEntity>> vacancies(String text, Integer page, Integer per_page)
  {
    if (dataCache == null)
      return Observable.error(new Exception("internal error"));
    else
    {
      StringBuilder file_name = new StringBuilder(VacancyEntity.class.getSimpleName());
      file_name.append("_p_");
      file_name.append(page.toString());
      file_name.append("_pp_");
      file_name.append(per_page.toString());

      return dataCache.get(file_name.toString(), DataCache.ElementType.TYPE_VACANCY_LIST)
              .map(new Func1<Collection, List<VacancyEntity>>()
              {
                @Override
                public List<VacancyEntity> call(Collection collection)
                {
                  List<VacancyEntity> list = new ArrayList<>();
                  if (collection != null)
                  {
                    Iterator it = collection.iterator();
                    while (it.hasNext())
                    {
                      Object item = it.next();
                      if (item != null && item instanceof VacancyEntity)
                      {
                        list.add((VacancyEntity)item);
                      }
                    }
                  }
                  return list;
                }
              });

    }

  }

  @Override
  public Observable<VacancyEntity> vacancy(Long vacancyId)
  {
    throw new UnsupportedOperationException("Operation is not available!!!");
  }

  @Override
  public Observable<Response<ListWrapper<SuggestionEntity>>> suggestions(String text)
  {
    throw new UnsupportedOperationException("Operation is not available!!!");
  }

  @Override
  public Observable<Uri> dataExport(String containerName, String data)
  {
    return this.dataCache.put(containerName, data);
  }
}
