package com.msgeekay.hhtest.datalayer.db;

import com.msgeekay.hhtest.model.data.ListWrapper;
import com.msgeekay.hhtest.model.data.SuggestionEntity;
import com.msgeekay.hhtest.model.data.VacancyEntity;

import java.util.List;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by grigoriykatz on 02/10/17.
 */

public interface DBApi
{

  Observable<List<VacancyEntity>> vacancies(String text, Integer page, Integer per_page);

  Observable<VacancyEntity> vacancy(final Long vacancyId);

  //Observable<Response<ListWrapper<SuggestionEntity>>> suggestions(String text);

}