package com.msgeekay.hhtest.datalayer.repo;

import android.net.Uri;

import com.msgeekay.hhtest.datalayer.repo.datastore.DataStore;
import com.msgeekay.hhtest.datalayer.repo.datastore.DataStoreFactory;
import com.msgeekay.hhtest.domainlayer.repo.Repository;
import com.msgeekay.hhtest.model.Suggestion;
import com.msgeekay.hhtest.model.Vacancy;
import com.msgeekay.hhtest.model.data.ListWrapper;
import com.msgeekay.hhtest.model.data.VacancyEntity;
import com.msgeekay.hhtest.model.data.mapping.SuggestionEntityDataMapper;
import com.msgeekay.hhtest.model.data.mapping.VacancyEntityDataMapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;


/**
 * Created by grigoriykatz on 17/09/17.
 */
@Singleton
public class DataRepository implements Repository
{
  private final DataStoreFactory dataStoreFactory;

  @Inject
  public DataRepository(DataStoreFactory dataStoreFactory)
  {
    this.dataStoreFactory = dataStoreFactory;
  }


  @Override
  public Observable<List<Vacancy>> vacancies(String text, Integer page, Integer per_page)
  {
    final DataStore ds = dataStoreFactory.createVacanciesStore();
    return ds.vacancies(text, page, per_page)
            .map(vacancyEntity -> VacancyEntityDataMapper.transform(vacancyEntity));

  }

  @Override
  public Observable<Vacancy> vacancy(Long vacancyId)
  {
    final DataStore ds = dataStoreFactory.createVacanciesStore();
    return ds.vacancy(vacancyId).map(vacancyEntity -> VacancyEntityDataMapper.transform(vacancyEntity));
  }

  @Override
  public Observable<List<Suggestion>> suggestions(String text)
  {
    final DataStore ds = dataStoreFactory.createVacanciesStore();
    return ds.suggestions(text).map(suggestionEntity -> SuggestionEntityDataMapper.transform(suggestionEntity.body()));
  }
}
