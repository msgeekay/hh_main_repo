package com.msgeekay.hhtest.datalayer.cache;

import android.content.Context;
import android.net.Uri;

import com.msgeekay.hhtest.datalayer.cache.serializer.JsonSerializer;
import com.msgeekay.hhtest.datalayer.exception.VacsException;
import com.msgeekay.hhtest.datalayer.exception.UserNotFoundException;
import com.msgeekay.hhtest.domainlayer.executor.ThreadExecutor;

import java.io.File;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by grigoriykatz on 17/09/17.
 */

@Singleton
public class DataCacheImpl implements DataCache
{
  public Context context;
  public FileManager fileManager;
  private final ThreadExecutor threadExecutor;

  @Inject
  public DataCacheImpl(Context context, FileManager fileManager, ThreadExecutor executor)
  {
    this.context = context;
    this.fileManager = fileManager;
    this.threadExecutor = executor;
  }

  @Override
  public Observable<Collection> get(String containerName, int elementType)
  {
    final JsonSerializer serializer = JsonSerializer.newInstance(elementType);

    return Observable.create(subscriber -> {
      File entityFile = this.buildFile(containerName);
      String fileContent = this.fileManager.readFileContent(entityFile);
      Collection itemCollection = serializer.deserialize(fileContent, -1);

      if (itemCollection != null) {
        subscriber.onNext(itemCollection);
        subscriber.onCompleted();
      } else {
        subscriber.onError(new UserNotFoundException());
      }
    });
  }

  @Override
  public Observable<Uri> put(String containerName, String content)
  {
    return Observable.create(new Observable.OnSubscribe<Uri>()
    {
      @Override
      public void call(Subscriber<? super Uri> subscriber)
      {
        Uri retVal = DataCacheImpl.this.fileManager.writeData(containerName, content);

        if (retVal != null) {
          subscriber.onNext(retVal);
          subscriber.onCompleted();
        } else {
          subscriber.onError(new VacsException("Smth went wrong. Pls look into logs"));
        }

      }
    });

  }

  @Override
  public Void put(String containerName, List list, int elementType)
  {
    final JsonSerializer serializer = JsonSerializer.newInstance(elementType);

    if (list != null) {
      File entityListFile = this.buildFile(containerName);
      if (!isCached(containerName)) {
        String jsonString = serializer.serialize(list);
        this.executeAsynchronously(new CacheWriter(this.fileManager, entityListFile,
                jsonString));
        //setLastCacheUpdateTimeMillis();
      }
    }

    return null;
  }

  @Override
  public boolean isCached(String containerName)
  {
    File entitiyFile = this.buildFile(containerName);
    return this.fileManager.exists(entitiyFile);
  }

  @Override
  public boolean isExpired()
  {
    return false;
  }

  @Override
  public void evictAll()
  {
    this.executeAsynchronously(new CacheEvictor(this.fileManager, new File(FileManager.getHomeFolder())));
  }


  public File buildFile(String containerName)
  {
    return this.fileManager.buildFile(containerName);
  }

  /**
   * Executes a {@link Runnable} in another Thread.
   *
   * @param runnable {@link Runnable} to execute
   */
  private void executeAsynchronously(Runnable runnable) {
    this.threadExecutor.execute(runnable);
  }

  /**
   * {@link Runnable} class for writing to disk.
   */
  private static class CacheWriter implements Runnable {
    private final FileManager fileManager;
    private final File fileToWrite;
    private final String fileContent;

    CacheWriter(FileManager fileManager, File fileToWrite, String fileContent) {
      this.fileManager = fileManager;
      this.fileToWrite = fileToWrite;
      this.fileContent = fileContent;
    }

    @Override public void run() {
      this.fileManager.writeToFile(fileToWrite, fileContent);
    }
  }

  /**
   * {@link Runnable} class for evicting all the cached files
   */
  private static class CacheEvictor implements Runnable {
    private final FileManager fileManager;
    private final File cacheDir;

    CacheEvictor(FileManager fileManager, File cacheDir) {
      this.fileManager = fileManager;
      this.cacheDir = cacheDir;
    }

    @Override public void run() {
      this.fileManager.clearDirectory(this.cacheDir);
    }
  }
}
