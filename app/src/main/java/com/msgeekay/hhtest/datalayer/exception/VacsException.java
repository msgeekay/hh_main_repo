package com.msgeekay.hhtest.datalayer.exception;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public class VacsException extends Exception
{
  public VacsException() {
    super();
  }

  public VacsException(final String message) {
    super(message);
  }

  public VacsException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public VacsException(final Throwable cause) {
    super(cause);
  }
}
