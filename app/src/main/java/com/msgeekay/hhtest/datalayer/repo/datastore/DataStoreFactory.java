package com.msgeekay.hhtest.datalayer.repo.datastore;

import android.content.Context;

import com.msgeekay.hhtest.datalayer.cache.DataCache;
import com.msgeekay.hhtest.datalayer.db.DBApi;
import com.msgeekay.hhtest.datalayer.db.DBApiImpl;
import com.msgeekay.hhtest.datalayer.net.BaseApi;
import com.msgeekay.hhtest.presentationlayer.Preferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Retrofit;

/**
 * Created by grigoriykatz on 18/09/17.
 *
 * Factory for different store implementations
 */

@Singleton
public class DataStoreFactory
{
  public final static int STORE_TYPE_NET   = 0;
  public final static int STORE_TYPE_DISK  = 1;
  public final static int STORE_TYPE_DB  = 2;

  private final Context context;
  private final DataCache dataCache;
  private final BaseApi net;
  private final Preferences prefs;
  private final Retrofit retrofit;
  private final DBApi dbApi;

  private final int defaultCachingType = STORE_TYPE_DB;


  @Inject
  public DataStoreFactory(Context context, DataCache dataCache, BaseApi net,
                            Preferences preferences, Retrofit retrofit, DBApi dbApi)
  {
    if (context == null) {
      throw new IllegalArgumentException("Constructor parameters cannot be null!!!");
    }
    this.context = context.getApplicationContext();
    this.dataCache = dataCache;
    this.net = net;
    this.prefs = preferences;
    this.retrofit = retrofit;
    this.dbApi = dbApi;
  }

  /**
   * Create {@link DataStore} for notes.
   */
  public DataStore createVacanciesStore()
  {
    return createVacanciesStore(STORE_TYPE_NET);
  }


  public DataStore createVacanciesStore(int storeType)
  {
    DataStore notesDataStore;

    //TODO: great place to put various implementations
    //TODO: depending on various conditions

    if (Preferences.isInternetConnected(context))
    {
      if (storeType == STORE_TYPE_NET)
      {
        notesDataStore = createNetVacanciesDataStore();
      }
      else if (storeType == STORE_TYPE_DISK)
      {
        notesDataStore = createDiskVacanciesDataStore();
      }
      else
        notesDataStore = createNetVacanciesDataStore();
    }
    else
    {
      if (defaultCachingType == STORE_TYPE_DB)
        notesDataStore = createDBVacanciesDataStore();
      else
        notesDataStore = createDiskVacanciesDataStore();
    }

    return notesDataStore;
  }

  private DataStore createNetVacanciesDataStore()
  {
    return new VacanciesApiDataStore(this.net, this.dataCache, this.retrofit, this.dbApi);
  }

  private DataStore createDiskVacanciesDataStore()
  {
    return new VacanciesDiskDataStore(this.dataCache);
  }

  private DataStore createDBVacanciesDataStore()
  {
    return new VacanciesDBDataStore(this.dbApi);
  }
}
