package com.msgeekay.hhtest.datalayer.cache.serializer;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.msgeekay.hhtest.datalayer.cache.DataCache;
import com.msgeekay.hhtest.model.Vacancy;
import com.msgeekay.hhtest.model.data.VacancyEntity;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.MediaType;
import okhttp3.ResponseBody;


/**
 * Class user as Serializer/Deserializer for user entities.
 */
@Singleton
public class JsonSerializer<T> {

  public static JsonSerializer newInstance(Type type)
  {
    return new JsonSerializer(type);
  }

  public static JsonSerializer newInstance(int type)
  {
    if (type == DataCache.ElementType.TYPE_VACANCY_LIST
        || type == DataCache.ElementType.TYPE_VACANCY_DET)
      return new JsonSerializer(VacancyEntity.class);
    else
      return null;
  }

  private final Gson gson = new Gson();
  private T type;

  @Inject
  public JsonSerializer(T type) {
    this.type = type;
  }

  /**
   * Serialize an object to Json.
   *
   * @param entity {@link T} to serialize.
   */
  public String serialize(T entity) {
    //String jsonString = gson.toJson(entity, T.class);
    String jsonString = gson.toJson(entity, type.getClass());
    return jsonString;
  }

  /**
   * Deserialize a json representation of an object.
   *
   * @param jsonString A json string to deserialize.
   * @return {@link T}
   */
  public T deserialize(String jsonString) {
    //T entity = gson.fromJson(jsonString, UserEntity.class);
    T entity = (T)gson.fromJson(jsonString, type.getClass());
    return entity;
  }

  /**
   * Serialize an object to Json.
   *
   * @param entityList {@link List <entityList>} to serialize.
   */
  public String serialize_old(List<T> entityList) {

    Type listOfTestObject = new TypeToken<List<T>>(){}.getType();
    String jsonString = gson.toJson(entityList, listOfTestObject);


    return jsonString;
  }

  public String serialize(List<T> entityList) {

    Type listOfTestObject = new TypeToken<List<T>>(){}.getType();
    String jsonString = gson.toJson(entityList, listOfTestObject);


    return jsonString;
  }

  /**
   * Deserialize a json representation of an object collection.
   *
   * @param jsonString A json string to deserialize.
   * @return {@link List<T>}
   */
  public List<T> deserialize_old(String jsonString, int flg)
  {

    Type listOfTestObject = new TypeToken<List<T>>(){}.getType();
    List<T> entityList = gson.fromJson(jsonString, listOfTestObject);

    return entityList;
  }

  public List<T> deserialize(String jsonString, int flg)
  {

//    Type listOfTestObject = new TypeToken<List<T>>(){}.getType();
//    List<T> entityList = gson.fromJson(jsonString, listOfTestObject);
    List<T> entityList = new ArrayList<>();
    TypeAdapter<?> adapter = gson.getAdapter(TypeToken.get(entityList.getClass()));
    ResponseBody rb = ResponseBody.create(MediaType.parse("application/json") , jsonString);
    JsonReader jsonReader = gson.newJsonReader(rb.charStream());
    try {
      entityList =  (List<T>)adapter.read(jsonReader);
    } catch (Exception ex) {

      //do nothing
    } finally {
      rb.close();
    }

    return entityList;
  }
}
