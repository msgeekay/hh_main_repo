package com.msgeekay.hhtest.datalayer.image.implv1;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class ImgCache
{
  private static ImgCache instance;
  private LruCache<String, Bitmap> lru;

  private ImgCache() {

    lru = new LruCache<String, Bitmap>(1024);

  }

  public static ImgCache instance()
  {
    return getInstance();
  }

  public static ImgCache getInstance() {

    if (instance == null) {

      instance = new ImgCache();
    }

    return instance;

  }

  public LruCache<String, Bitmap> getLru() {
    return lru;
  }

}