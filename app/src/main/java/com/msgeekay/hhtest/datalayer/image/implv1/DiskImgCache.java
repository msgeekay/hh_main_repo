package com.msgeekay.hhtest.datalayer.image.implv1;

/**
 * Created by grigoriykatz on 03/10/17.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.WorkerThread;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ImageView;

import com.jakewharton.disklrucache.DiskLruCache;
import com.msgeekay.hhtest.R;
import com.msgeekay.hhtest.datalayer.utils.ImageUtils;
import com.msgeekay.hhtest.presentationlayer.MyApp;
import com.msgeekay.hhtest.presentationlayer.utils.Parameters;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * Created by grigoriykatz on 11/07/17.
 *
 * Helper class to work with Jake Wharton's DiskLruCache in context of image caching.
 * Helper methods also use memory Lru cache to store related cached images in.
 */

public class DiskImgCache
{
  public static final int DEFAULT_DISK_CACHE_SIZE = 1024 * 1024 * 10; // 10MB
  public static final Bitmap.CompressFormat DEFAULT_COMRESS_FORMAT = Bitmap.CompressFormat.JPEG;
  public static final int DEFAULT_COMPRESS_QUALITY = 70;

  private static final String TAG = DiskImgCache.class.getName();

  private DiskLruCache mDiskCache;
  private Bitmap.CompressFormat mCompressFormat = Bitmap.CompressFormat.JPEG;
  private int mCompressQuality = 70;
  private static final int APP_VERSION = 1;
  private static final int VALUE_COUNT = 1;

  private static DiskImgCache instance;

  public static DiskImgCache getInstance() {

    if (instance == null) {

      instance = new DiskImgCache(MyApp.getInstance(), "diskimgcache",
              DEFAULT_DISK_CACHE_SIZE,
              DEFAULT_COMRESS_FORMAT,
              DEFAULT_COMPRESS_QUALITY);
    }

    return instance;

  }



  private DiskImgCache(Context context, String uniqueName, int diskCacheSize,
                       Bitmap.CompressFormat compressFormat, int quality ) {
    try {
      final File diskCacheDir = getDiskCacheDir(context, uniqueName);
      mDiskCache = DiskLruCache.open( diskCacheDir, APP_VERSION, VALUE_COUNT, diskCacheSize );
      mCompressFormat = compressFormat;
      mCompressQuality = quality;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private boolean writeBitmapToFile( Bitmap bitmap, DiskLruCache.Editor editor )
          throws IOException, FileNotFoundException
  {
    OutputStream out = null;
    try {
      out = new BufferedOutputStream( editor.newOutputStream( 0 ), ImageUtils.IO_BUFFER_SIZE );
      return bitmap.compress( mCompressFormat, mCompressQuality, out );
    } finally {
      if ( out != null ) {
        out.close();
      }
    }
  }

  private File getDiskCacheDir(Context context, String uniqueName) {

    // Check if media is mounted or storage is built-in, if so, try and use external cache dir
    // otherwise use internal cache dir
    final String cachePath =
            Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
                    !ImageUtils.isExternalStorageRemovable() ?
                    ImageUtils.getExternalCacheDir(context).getPath() :
                    context.getCacheDir().getPath();

    return new File(cachePath + File.separator + uniqueName);
  }

  @WorkerThread
  public void put( String key, Bitmap data ) {

    DiskLruCache.Editor editor = null;
    try {
      editor = mDiskCache.edit( key );
      if ( editor == null ) {
        return;
      }

      if( writeBitmapToFile( data, editor ) ) {
        mDiskCache.flush();
        editor.commit();
        if ( Parameters.DEBUG ) {
          Log.d( TAG, "image put on disk cache " + key );
        }
      } else {
        editor.abort();
        if ( Parameters.DEBUG ) {
          Log.d( TAG, "ERROR on: image put on disk cache " + key );
        }
      }
    } catch (IOException e) {
      if ( Parameters.DEBUG ) {
        Log.d( TAG, "ERROR on: image put on disk cache " + key );
      }
      try {
        if ( editor != null ) {
          editor.abort();
        }
      } catch (IOException ignored) {
      }
    }

  }

  @WorkerThread
  public Bitmap getBitmap( String key ) {

    Bitmap bitmap = null;
    DiskLruCache.Snapshot snapshot = null;
    try {

      snapshot = mDiskCache.get( key );
      if ( snapshot == null ) {
        return null;
      }
      final InputStream in = snapshot.getInputStream( 0 );
      if ( in != null ) {
        final BufferedInputStream buffIn =
                new BufferedInputStream( in, ImageUtils.IO_BUFFER_SIZE );
        bitmap = BitmapFactory.decodeStream( buffIn );
      }
    } catch ( IOException e ) {
      e.printStackTrace();
    } finally {
      if ( snapshot != null ) {
        snapshot.close();
      }
    }

    if ( Parameters.DEBUG ) {
      Log.d( TAG, bitmap == null ? "" : "image read from disk " + key);
    }

    return bitmap;

  }

  public boolean containsKey( String userkey )
  {
    String key = hashKeyForDisk(userkey);

    boolean contained = false;
    DiskLruCache.Snapshot snapshot = null;
    try {
      snapshot = mDiskCache.get( key );
      contained = snapshot != null;
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if ( snapshot != null ) {
        snapshot.close();
      }
    }

    return contained;

  }

  public void clearCache() {
    if ( Parameters.DEBUG ) {
      Log.d( TAG, "disk cache CLEARED");
    }
    try {
      mDiskCache.delete();
    } catch ( IOException e ) {
      e.printStackTrace();
    }
  }

  public File getCacheFolder() {
    return mDiskCache.getDirectory();
  }

  /**
   * A hashing method that changes a string (like a URL) into a hash suitable for using as a
   * disk filename.
   */
  public static String hashKeyForDisk(String key) {
    String cacheKey;
    try {
      final MessageDigest mDigest = MessageDigest.getInstance("MD5");
      mDigest.update(key.getBytes());
      cacheKey = bytesToHexString(mDigest.digest());
    } catch (NoSuchAlgorithmException e) {
      cacheKey = String.valueOf(key.hashCode());
    }
    return cacheKey;
  }

  private static String bytesToHexString(byte[] bytes) {
    // http://stackoverflow.com/questions/332079
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < bytes.length; i++) {
      String hex = Integer.toHexString(0xFF & bytes[i]);
      if (hex.length() == 1) {
        sb.append('0');
      }
      sb.append(hex);
    }
    return sb.toString();
  }

  /**
   * Helper methods to use cache with
   *
   */

  public void addImageToCache(final String url, final Bitmap image)
  {
    (new Thread(new Runnable()
    {
      @Override
      public void run()
      {
        ImgCache.getInstance().getLru().put(url, image);

        String key = hashKeyForDisk(url);
        put(key, image);
      }
    })).start();
  }

  public void getImageFromCache(final ImageView view, final String url)
  {
    new AsyncTask<Void, Void, Bitmap>()
    {
      private WeakReference<ImageView> weakReference;
      private String key;
      @Override
      protected void onPreExecute()
      {
        super.onPreExecute();
        weakReference = new WeakReference<ImageView>(view);


      }

      @Override
      protected Bitmap doInBackground(Void... params)
      {
        key = hashKeyForDisk(url);
        Bitmap bmp = getBitmap(key);
        if (bmp != null)
          ImgCache.getInstance().getLru().put(url, bmp);
        return bmp;
      }

      @Override
      protected void onPostExecute(Bitmap bitmap)
      {
        super.onPostExecute(bitmap);
        if (bitmap == null)
        {
          if (weakReference != null && weakReference.get() != null)
          {
            weakReference.get().setImageDrawable(ContextCompat.getDrawable(MyApp.getInstance(),
                    R.drawable.default_profile_background));
          }

        }
        else
        {
          if (weakReference != null && weakReference.get() != null)
          {
            weakReference.get().setImageBitmap(bitmap);
          }
        }
      }
    }.execute();


  }
}
