package com.msgeekay.hhtest.datalayer.cache;

import android.net.Uri;

import java.util.Collection;
import java.util.List;

import rx.Observable;

/**
 * Created by grigoriykatz on 19/09/17.
 */

public interface DataCache
{
  public final class ElementType
  {
    public static final int TYPE_VACANCY_LIST = 0;
    public static final int TYPE_VACANCY_DET = 1;
  }

  Observable<Collection> get(String containerName, int elementType);

  Observable<Uri> put(String containerName, String content);

  Void put(String containerName, List list, int elementType);

  boolean isCached(final String containerName);

  /**
   * Checks if the cache is expired.
   *
   * @return true, the cache is expired, otherwise false.
   */
  boolean isExpired();

  /**
   * Evict all elements of the cache.
   */
  void evictAll();
}
