package com.msgeekay.hhtest.datalayer.db;

import com.msgeekay.hhtest.datalayer.exception.VacsException;
import com.msgeekay.hhtest.model.data.VacancyEntity;
import com.msgeekay.hhtest.model.data.VacancyEntity_Table;
import com.raizlabs.android.dbflow.sql.language.Operator;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by grigoriykatz on 04/10/17.
 */

public class DBApiImpl implements DBApi
{
  @Override
  public Observable<List<VacancyEntity>> vacancies(String text, Integer page, Integer per_page)
  {
    return Observable.create(new Observable.OnSubscribe<List<VacancyEntity>>()
    {
      @Override
      public void call(Subscriber<? super List<VacancyEntity>> subscriber)
      {
        List<VacancyEntity> list = new ArrayList<>();
        try
        {
          Operator.In in = Operator.op(VacancyEntity_Table.page.getNameAlias()).in(page);
          Operator.In in2 = Operator.op(VacancyEntity_Table.per_page.getNameAlias()).in(per_page);
          list = SQLite.select()
                  .from(VacancyEntity.class)
                  .where(in).and(in2)
                  .orderBy(VacancyEntity_Table.timestamp.getNameAlias(), false)
                  .queryList();

          //list = _list.getAll();

          if (list != null)
          {
            subscriber.onNext(list);
            subscriber.onCompleted();
          }
          else
          {
            subscriber.onError(new VacsException());
          }
        }
        catch (Exception ex)
        {
          subscriber.onError(new VacsException(ex.getCause()));
        }

      }
    });
  }

  @Override
  public Observable<VacancyEntity> vacancy(final Long vacancyId)
  {

    return Observable.create(new Observable.OnSubscribe<VacancyEntity>()
    {
      @Override
      public void call(Subscriber<? super VacancyEntity> subscriber)
      {
        VacancyEntity entity = null;
        try
        {
          //Condition.In in = Condition.column(NoteEntity_Table.noteId.getNameAlias()).in(noteId);
          Operator.In in = Operator.op(VacancyEntity_Table.id.getNameAlias()).in(vacancyId);
          entity = SQLite.select()
                  .from(VacancyEntity.class)
                  .where(in)
                  .querySingle();

          if (entity != null)
          {
            subscriber.onNext(entity);
            subscriber.onCompleted();
          }
          else
          {
            subscriber.onError(new VacsException());
          }
        }
        catch (Exception ex)
        {
          subscriber.onError(new VacsException(ex.getCause()));
        }
      }
    });
  }

}
