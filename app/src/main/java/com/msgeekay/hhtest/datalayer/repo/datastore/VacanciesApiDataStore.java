package com.msgeekay.hhtest.datalayer.repo.datastore;

import android.net.Uri;
import android.util.Log;

import com.msgeekay.hhtest.datalayer.cache.DataCache;
import com.msgeekay.hhtest.datalayer.db.DBApi;
import com.msgeekay.hhtest.datalayer.db.VacsDatabase;
import com.msgeekay.hhtest.datalayer.net.BaseApi;
import com.msgeekay.hhtest.model.Suggestion;
import com.msgeekay.hhtest.model.data.ListWrapper;
import com.msgeekay.hhtest.model.data.SuggestionEntity;
import com.msgeekay.hhtest.model.data.VacancyEntity;
import com.msgeekay.hhtest.model.data.VacancyEntity_Table;
import com.msgeekay.hhtest.presentationlayer.utils.Parameters;
import com.raizlabs.android.dbflow.annotation.provider.ContentUri;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.Operator;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import kotlin.jvm.functions.Function1;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class VacanciesApiDataStore implements DataStore
{
  private final static String TAG = VacanciesApiDataStore.class.getName();
  private BaseApi network;
  private DataCache dataCache;
  private Retrofit retrofit;
  private DBApi dbApi;

  public VacanciesApiDataStore(BaseApi network, DataCache dataCache, Retrofit retrofit, DBApi dbApi)
  {
    this.network = network;
    this.dataCache = dataCache;
    this.retrofit = retrofit;
    this.dbApi = dbApi;
  }

  @Override
  //public Observable<Response<ListWrapper<VacancyEntity>>> vacancies(String text, Integer page, Integer per_page)
  public Observable<List<VacancyEntity>> vacancies(String text, Integer page, Integer per_page)
  //public Observable<ListWrapper<VacancyEntity>> vacancies(String text, Integer page, Integer per_page)
  {
    if (network == null)
      return Observable.error(new Exception("internal error"));

    final Action1<List<VacancyEntity>> action_extra =
          vacancyEntityList ->
          {
            if (vacancyEntityList != null && vacancyEntityList.size() > 0)
            {
              if (Parameters.DEBUG)
              {
                Log.d(TAG, Integer.toString(vacancyEntityList.size()));
              }
              if (text != null && text.length() > 0)
                return;


              StringBuilder file_name = new StringBuilder(VacancyEntity.class.getSimpleName());
              file_name.append("_p_");
              file_name.append(page.toString());
              file_name.append("_pp_");
              file_name.append(per_page.toString());

              dataCache.put(file_name.toString(), vacancyEntityList, DataCache.ElementType.TYPE_VACANCY_LIST);

            }
          };

    final Action1<List<VacancyEntity>> action_extra_db =
            vacancyEntityList ->
            {
              if (vacancyEntityList != null && vacancyEntityList.size() > 0)
              {
                if (Parameters.DEBUG)
                {
                  Log.d(TAG, Integer.toString(vacancyEntityList.size()));
                }
                if (text != null && text.length() > 0)
                  return;

                try
                {

                  SQLite.delete(VacancyEntity.class).where(VacancyEntity_Table.page.eq(page.longValue())).async().execute();

                  for (VacancyEntity e : vacancyEntityList)
                  {
                    if (e == null)
                      continue;
                    String url = e.getImageURI();
                    if (url == null || (url != null && url.length() == 0))
                      url = e.getImageURIFromEmployer();
                    VacancyEntity ve = new VacancyEntity(e.getId(), e.getDescription(), e.getName(),
                                            url);
                    ve.page = page;
                    ve.per_page = per_page;
                    ve.save();
                  }

                }
                catch (Exception ex)
                {
                  if (Parameters.DEBUG)
                    ex.printStackTrace();
                }

              }
            };

    return network.getVacancies(text, page, per_page)
          .map(new Func1<Response<ListWrapper<VacancyEntity>>, List<VacancyEntity>>()
          {
            @Override
            public List<VacancyEntity> call(final Response<ListWrapper<VacancyEntity>> response)
            {
              if (response.isSuccessful())
              {
                ListWrapper<VacancyEntity> content = null;
                List<VacancyEntity> veList = new ArrayList<>();
                try
                {
                  content = response.body();
                  veList.addAll(content.items);
                }
                catch (Exception ex)
                {
                  ex.printStackTrace();
                  //return;
                }

                return veList;
              }
              else
              {
                //throw Exception();
                return null;
              }
            }
          })
            .doOnNext(action_extra_db);
            //.doOnNext(action_extra);

  }

  @Override
  public Observable<VacancyEntity> vacancy(Long vacancyId)
  {
    if (network == null)
      return Observable.error(new Exception("internal error"));

    return network.getVacancyById(vacancyId).map(new Func1<Response<VacancyEntity>, VacancyEntity>()
    {
      @Override
      public VacancyEntity call(Response<VacancyEntity> vacancyEntityResponse)
      {
        if (vacancyEntityResponse.isSuccessful())
        {
          VacancyEntity v = vacancyEntityResponse.body();
          if (v != null)
            return v;
          else
            return null;
        }
        else
        {
          return null;
        }
      }
    });
  }

  @Override
  public Observable<Response<ListWrapper<SuggestionEntity>>> suggestions(String text)
  {
    if (network == null)
      return Observable.error(new Exception("internal error"));

    return network.getSearchSuggestions(text);
  }

  @Override
  public Observable<Uri> dataExport(String containerName, String data)
  {
    return null;
  }
}
