package com.msgeekay.hhtest.presentationlayer.ui.main.list;

import com.arellomobile.mvp.MvpView;
import com.msgeekay.hhtest.domainlayer.exception.ErrorBundle;
import com.msgeekay.hhtest.model.Vacancy;

import java.util.List;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public interface CommonView extends MvpView
{
  void setVacancies(List<Vacancy> vacancyList);
  void updateListener(VacanciesAdapterList.OnVacancyClickListener listener);

  public void hideViewLoading();
  public void showErrorMessage(ErrorBundle errorBundle);

  void refreshData();

}
