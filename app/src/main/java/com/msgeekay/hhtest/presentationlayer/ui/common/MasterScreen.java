package com.msgeekay.hhtest.presentationlayer.ui.common;

import com.arellomobile.mvp.MvpPresenter;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public interface MasterScreen
{
  void setListPresenter(MvpPresenter presenter);
  void setDetailPresenter(MvpPresenter presenter);
}
