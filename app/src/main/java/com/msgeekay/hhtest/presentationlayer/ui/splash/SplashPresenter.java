package com.msgeekay.hhtest.presentationlayer.ui.splash;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.msgeekay.hhtest.presentationlayer.MyApp;

import javax.inject.Inject;

/**
 * Created by grigoriykatz on 22/09/17.
 */

@InjectViewState
public class SplashPresenter extends MvpPresenter<SplashView>
{

  @Inject
  public SplashPresenter()
  {

    MyApp.get().getApplicationComponent().inject(this);
  }


}
