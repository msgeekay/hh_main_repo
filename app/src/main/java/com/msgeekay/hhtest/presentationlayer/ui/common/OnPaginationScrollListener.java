package com.msgeekay.hhtest.presentationlayer.ui.common;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by grigoriykatz on 05/10/17.
 */

public abstract class OnPaginationScrollListener extends RecyclerView.OnScrollListener {

  private static final int MIN_VIEW_COUNT = 8;

  private LinearLayoutManager layoutManager;

  private long max;

  private boolean isLoading = false;

  public OnPaginationScrollListener(LinearLayoutManager layoutManager, long max) {
    this.layoutManager = layoutManager;
    this.max = max == 0 ? Long.MAX_VALUE : max;
  }

  public OnPaginationScrollListener(LinearLayoutManager layoutManager) {
    this.layoutManager = layoutManager;
    max = Long.MAX_VALUE;
  }

  @Override
  public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
    super.onScrollStateChanged(recyclerView, newState);
    if (layoutManager.getItemCount() < max
            && layoutManager.getItemCount() - layoutManager.findLastVisibleItemPosition() < MIN_VIEW_COUNT) {
      if (!isLoading) {
        isLoading = true;
        load();
      }
    }
  }

  public abstract void load();

  public void loadingComplete() {
    isLoading = false;
  }

}

