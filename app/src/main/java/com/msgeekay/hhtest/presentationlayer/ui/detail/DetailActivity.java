package com.msgeekay.hhtest.presentationlayer.ui.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.msgeekay.hhtest.R;
import com.msgeekay.hhtest.presentationlayer.ui.common.CustomPresenterFactory;
import com.msgeekay.hhtest.presentationlayer.ui.common.MasterScreen;
import com.msgeekay.hhtest.presentationlayer.ui.common.mvp.BaseMvpActivity;
import com.msgeekay.hhtest.presentationlayer.ui.detail.detailscreen.DetailFragment;
import com.msgeekay.hhtest.presentationlayer.ui.detail.detailscreen.DetailScreenPresenter;
import com.msgeekay.hhtest.presentationlayer.ui.main.MainActivity;
import com.msgeekay.hhtest.presentationlayer.ui.main.MainPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class DetailActivity extends BaseMvpActivity implements DetailView, MasterScreen
{
  public static final String EXTRA_VAC_ID = "EXTRA_VAC_ID";

  public static Intent getCallingIntent(Context context, long vacancyId)
  {
    Intent callingIntent = new Intent(context, DetailActivity.class);
    callingIntent.putExtra(EXTRA_VAC_ID, vacancyId);
    return callingIntent;
  }

  @BindView(R.id.toolbar)
  Toolbar toolbar;

  @InjectPresenter(type = PresenterType.GLOBAL)
  DetailPresenter mPresenter;

  DetailScreenPresenter detailScreenPresenter;

  @ProvidePresenter(type = PresenterType.GLOBAL)
  DetailPresenter provideDetailPresenter() {
    CustomPresenterFactory cpf = new CustomPresenterFactory();
    return cpf.provideDetailPresenter();
  }

  private long vacancyId = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    Timberlg(getClass().getName(), "onCreate");
    setTheme(R.style.AppTheme_MainActivity);
    super.onCreate(savedInstanceState);
    //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    setContentView(R.layout.activity_detail);
    ButterKnife.bind(this);

    setSupportActionBar(toolbar);
    //noinspection ConstantConditions
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    if (getSupportActionBar() != null)
      getSupportActionBar().setDisplayShowTitleEnabled(false);

    Intent i = getIntent();
    vacancyId = i.getLongExtra(EXTRA_VAC_ID, 0);

    setupParts(savedInstanceState);

  }

  private void setupParts(Bundle savedInstanceState)
  {
    if(getSupportFragmentManager().findFragmentByTag(DetailFragment.TAG) == null) {
      getSupportFragmentManager()
              .beginTransaction()
              .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
              .replace(R.id.container_det, DetailFragment.newInstance(this), DetailFragment.TAG)
              //.addToBackStack(CommonFragment.TAG)
              .commit();
    }
  }

  @Override
  protected void onStart()
  {
    super.onStart();
  }

  @Override
  protected void onStop()
  {
    if (mPresenter != null)
      mPresenter.stop();

    super.onStop();
  }

  @Override
  public void permissionsCheckedOk()
  {

  }

  @Override
  public void setListPresenter(MvpPresenter presenter)
  {

  }

  @Override
  public void setDetailPresenter(MvpPresenter presenter)
  {
    this.detailScreenPresenter = (DetailScreenPresenter) presenter;
    if (this.detailScreenPresenter != null)
      this.detailScreenPresenter.updateDetailScreen(vacancyId);
  }
}
