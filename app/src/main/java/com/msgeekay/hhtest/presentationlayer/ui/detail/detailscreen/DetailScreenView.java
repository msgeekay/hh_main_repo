package com.msgeekay.hhtest.presentationlayer.ui.detail.detailscreen;

import com.arellomobile.mvp.MvpView;
import com.msgeekay.hhtest.domainlayer.exception.ErrorBundle;
import com.msgeekay.hhtest.model.Vacancy;

import java.util.List;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public interface DetailScreenView extends MvpView
{
  void setVacancy(Vacancy vacancy);

  public void hideViewLoading();
  public void showErrorMessage(ErrorBundle errorBundle);
}
