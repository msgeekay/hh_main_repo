package com.msgeekay.hhtest.presentationlayer.ui.main;

import android.widget.AbsListView;

import com.arellomobile.mvp.MvpView;
import com.msgeekay.hhtest.domainlayer.exception.DefaultErrorBundle;
import com.msgeekay.hhtest.presentationlayer.ui.main.list.VacanciesAdapterList;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public interface MainView extends MvpView
{
  void showErrorMessage(DefaultErrorBundle d);
  void setSoftKeyboardToOverlapBottomScreenParts();
  void setSoftKeyboardToOverlapBottomScreenPartsNOT();

  public void showSearchBG();
  public void hideSearchBG();
  public void requestFocusFromEditText();

}
