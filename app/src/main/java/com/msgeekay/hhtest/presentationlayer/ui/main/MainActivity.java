package com.msgeekay.hhtest.presentationlayer.ui.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.msgeekay.hhtest.R;
import com.msgeekay.hhtest.domainlayer.exception.DefaultErrorBundle;
import com.msgeekay.hhtest.model.Vacancy;
import com.msgeekay.hhtest.presentationlayer.ui.common.CustomPresenterFactory;
import com.msgeekay.hhtest.presentationlayer.ui.common.MasterScreen;
import com.msgeekay.hhtest.presentationlayer.ui.common.mvp.BaseMvpActivity;
import com.msgeekay.hhtest.presentationlayer.ui.detail.detailscreen.DetailFragment;
import com.msgeekay.hhtest.presentationlayer.ui.main.list.CommonFragment;
import com.msgeekay.hhtest.presentationlayer.ui.main.list.VacanciesAdapterList;
import com.msgeekay.hhtest.presentationlayer.ui.main.search.SearchBarFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public class MainActivity extends BaseMvpActivity implements MainView, MasterScreen
{
  private static final String TAG = MainActivity.class.getName();
  private static final int REQUEST_CODE_FOR_EDIT = 1000;

  public final class ViewVersion
  {
    public static final int VersionShort = 0;
    public static final int VersionDetailed = 1;
  }

  @BindView(R.id.search_toucheable_bg) View search_toucheable_bg;
  @BindView(R.id.main_layout) View main_layout;
  @BindView(R.id.toolbar) Toolbar toolbar;
  private SearchBarFragment searchBarFragment;

  public static Intent getCallingIntent(Context context)
  {
    Intent callingIntent = new Intent(context, MainActivity.class);
    return callingIntent;
  }

  @InjectPresenter(type = PresenterType.GLOBAL)
  MainPresenter mainPresenter;

  @ProvidePresenter(type = PresenterType.GLOBAL)
  MainPresenter provideMainPresenter() {
    CustomPresenterFactory cpf = new CustomPresenterFactory();
    return cpf.provideMainPresenter();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    Timberlg(getClass().getName(), "onCreate");
    setTheme(R.style.AppTheme_MainActivity);
    super.onCreate(savedInstanceState);
    //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    setSupportActionBar(toolbar);
    //noinspection ConstantConditions
    //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    if (getSupportActionBar() != null)
      getSupportActionBar().setDisplayShowTitleEnabled(false);
    getSupportActionBar().hide();

    //initSearch();

    setupParts(savedInstanceState);

  }

/**
  private ImageView iv;
  private TextView text;
  private AnimatedVectorDrawable searchToBar;
  private AnimatedVectorDrawable barToSearch;
  private float offset;
  private Interpolator interp;
  private int duration;
  private boolean expanded = false;

  private void initSearch()
  {
    iv = (ImageView) findViewById(R.id.search);
    text = (TextView) findViewById(R.id.text);
    searchToBar = (AnimatedVectorDrawable) getResources().getDrawable(R.drawable.anim_search_to_bar);
    barToSearch = (AnimatedVectorDrawable) getResources().getDrawable(R.drawable.anim_bar_to_search);
    interp = AnimationUtils.loadInterpolator(this, android.R.interpolator.linear_out_slow_in);
    duration = getResources().getInteger(R.integer.duration_bar);
    // iv is sized to hold the search+bar so when only showing the search icon, translate the
    // whole view left by half the difference to keep it centered
    offset = -71f * (int) getResources().getDisplayMetrics().scaledDensity;
    iv.setTranslationX(offset);
  }

  public void animate(View view) {

    if (!expanded) {
      iv.setImageDrawable(searchToBar);
      searchToBar.start();
      iv.animate().translationX(0f).setDuration(duration).setInterpolator(interp);
      text.animate().alpha(1f).setStartDelay(duration - 100).setDuration(100).setInterpolator(interp);
    } else {
      iv.setImageDrawable(barToSearch);
      barToSearch.start();
      iv.animate().translationX(offset).setDuration(duration).setInterpolator(interp);
      text.setAlpha(0f);
    }
    expanded = !expanded;
  }

  ***/

  @Override
  protected void onPostCreate(@Nullable Bundle savedInstanceState)
  {
    super.onPostCreate(savedInstanceState);

  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_main, menu);
    return true;

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void permissionsCheckedOk()
  {

  }

  @Override
  protected void onStart()
  {
    super.onStart();

    if (mainPresenter != null)
      mainPresenter.start(this);

    searchBarFragment = (SearchBarFragment)SearchBarFragment.newInstance(this, mainPresenter);
    //searchResultFragment = ComplexSearchResultFragment.newInstance(mPresenter);
    initSearchFragment(searchBarFragment, false, searchBarFragment.getClass().getName());


  }

  @Override
  protected void onStop()
  {
    if (mainPresenter != null)
      mainPresenter.stop();

    super.onStop();
  }

  @Override
  public void setListPresenter(MvpPresenter presenter)
  {
    if (mainPresenter != null)
      mainPresenter.setListPresenter(presenter);
  }

  @Override
  public void setDetailPresenter(MvpPresenter presenter)
  {
    if (mainPresenter != null)
      mainPresenter.setDetailPresenter(presenter);
  }


  public void setupParts(Bundle savedInstanceState)
  {


    if (findViewById(R.id.container_det) != null)
    {
      if (mainPresenter != null)
        mainPresenter.setViewVersion(ViewVersion.VersionDetailed);
      if (savedInstanceState != null)
        return;

      initCommonFragment();
      initDetailFragment();
    }
    else
    {
      if (mainPresenter != null)
        mainPresenter.setViewVersion(ViewVersion.VersionShort);
      if (savedInstanceState != null)
        return;

      initCommonFragment();
    }

  }


  public void initCommonFragment()
  {
    Fragment f = getSupportFragmentManager().findFragmentByTag(CommonFragment.TAG);
    if (f == null) {
      getSupportFragmentManager()
              .beginTransaction()
              .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
              .replace(R.id.container, CommonFragment.newInstance(this), CommonFragment.TAG)
              //.addToBackStack(CommonFragment.TAG)
              .commit();
    }
    else
    {
      showFragment(f);
    }
  }

  public void initDetailFragment()
  {
    Fragment f = getSupportFragmentManager().findFragmentByTag(DetailFragment.TAG);
    if (f == null)
    {
      getSupportFragmentManager()
              .beginTransaction()
              .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
              .replace(R.id.container_det, DetailFragment.newInstance(this), DetailFragment.TAG)
              //.addToBackStack(CommonFragment.TAG)
              .commit();
    }
    else
    {
      showFragment(f);
    }
  }

  private void initSearchFragment(Fragment fragment, boolean isAddToBackStack, String typeName)
  {
    Fragment f = getSupportFragmentManager().findFragmentByTag(SearchBarFragment.TAG);
    if (f == null)
    {
      FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
      //tx.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
      //tx.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
      if (fragment instanceof SearchBarFragment)
        tx.replace(R.id.search_bar_container, fragment, SearchBarFragment.TAG);

      if (isAddToBackStack)
      {
        tx.addToBackStack(typeName);
      }
      tx.commitAllowingStateLoss();
      boolean res = getSupportFragmentManager().executePendingTransactions();

    }
    else
    {
      showFragment(f);
    }

  }

  private void showFragment(Fragment f)
  {
    getSupportFragmentManager()
            .beginTransaction()
            .setCustomAnimations(R.anim.dlg_fdin, R.anim.dlg_fdout)
            //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            //.replace(R.id.container, CommonFragment.newInstance(this), CommonFragment.TAG)
            //.addToBackStack(CommonFragment.TAG)
            .show(f)
            .commit();
  }

  public void superOnBackPressed() {
    super.onBackPressed();
  }

  @Override
  public void onBackPressed()
  {

    finish();
  }



  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    if (requestCode == REQUEST_CODE_FOR_EDIT)
    {
//      if (resultCode == SMthActivity.RESULT_CODE_OK && data != null && data.getExtras() != null)
//      {
//
//      }
    }
    else
      super.onActivityResult(requestCode, resultCode, data);
  }


  @Override
  public void showErrorMessage(DefaultErrorBundle d)
  {
    Toast.makeText(this, d.getErrorMessage(), Toast.LENGTH_SHORT).show();
  }

  @Override
  public void setSoftKeyboardToOverlapBottomScreenParts()
  {
    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    Log.d(MainActivity.class.getName(), "setSoftKeyboardToOverlapBottomScreenParts...");
  }

  @Override
  public void setSoftKeyboardToOverlapBottomScreenPartsNOT()
  {
    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
            | WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    Log.d(MainActivity.class.getName(), "setSoftKeyboardToOverlapBottomScreenPartsNOT...");
  }

  public void showSearchBG()
  {
    search_toucheable_bg.setVisibility(View.VISIBLE);
    search_toucheable_bg.setOnTouchListener(new View.OnTouchListener()
    {
      @Override
      public boolean onTouch(View v, MotionEvent event)
      {
        if (event.getAction() == MotionEvent.ACTION_DOWN)
        {

          Rect outRect_bg = new Rect();
          search_toucheable_bg.getGlobalVisibleRect(outRect_bg);

          if (outRect_bg.contains((int) event.getRawX(), (int) event.getRawY()))
          {
            requestFocusFromEditText();
            hideSearchBG();
//            if (searchBarFragment != null)
//              searchBarFragment.
          }
        }
        return true;

      }
    });
  }



  public void hideSearchBG()
  {
    search_toucheable_bg.setVisibility(View.GONE);
  }

  public void requestFocusFromEditText()
  {
    if (main_layout != null)
      main_layout.requestFocus();
  }
}
