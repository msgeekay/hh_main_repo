package com.msgeekay.hhtest.presentationlayer.ui.common;

import com.msgeekay.hhtest.presentationlayer.MyApp;
import com.msgeekay.hhtest.presentationlayer.ui.detail.DetailPresenter;
import com.msgeekay.hhtest.presentationlayer.ui.detail.detailscreen.DetailScreenPresenter;
import com.msgeekay.hhtest.presentationlayer.ui.main.list.CommonPresenter;
import com.msgeekay.hhtest.presentationlayer.ui.main.MainPresenter;
import com.msgeekay.hhtest.presentationlayer.ui.main.search.SearchBarPresenter;
import com.msgeekay.hhtest.presentationlayer.ui.splash.SplashPresenter;


import javax.inject.Inject;

/**
 * Created by grigoriykatz on 18/05/17.
 */

public class CustomPresenterFactory //implements PresenterFactory //extends PresenterStore
{
  @Inject
  SplashPresenter splashPresenter;

  @Inject
  MainPresenter mainPresenter;

  @Inject
  CommonPresenter commonPresenter;

  @Inject
  DetailPresenter detailPresenter;

  @Inject
  DetailScreenPresenter detailScreenPresenter;

  @Inject
  SearchBarPresenter searchBarPresenter;


  public CustomPresenterFactory()
  {
    MyApp.get().getApplicationComponent().inject(this);
  }

  public SplashPresenter provideSplashPresenter()
  {
    return splashPresenter;
  }

  public MainPresenter provideMainPresenter()
  {
    return mainPresenter;
  }

  public CommonPresenter provideCommonPresenter() { return commonPresenter; }

  public DetailPresenter provideDetailPresenter() { return detailPresenter; }

  public DetailScreenPresenter provideDetailScreenPresenter() { return detailScreenPresenter; }

  public SearchBarPresenter provideSearchBarPresenter() { return searchBarPresenter; }

}