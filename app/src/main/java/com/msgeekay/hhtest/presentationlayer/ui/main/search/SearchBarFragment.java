package com.msgeekay.hhtest.presentationlayer.ui.main.search;

import android.animation.Animator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.msgeekay.hhtest.R;
import com.msgeekay.hhtest.model.Suggestion;
import com.msgeekay.hhtest.presentationlayer.ui.common.CustomPresenterFactory;
import com.msgeekay.hhtest.presentationlayer.ui.common.mvp.BaseMvpFragment;
import com.msgeekay.hhtest.presentationlayer.ui.common.views.DelayAutoCompleteTextView;
import com.msgeekay.hhtest.presentationlayer.ui.main.MainPresenter;
import com.msgeekay.hhtest.presentationlayer.utils.CustomUtils;

import butterknife.BindView;
import rx.functions.Action0;
import rx.schedulers.Schedulers;

/**
 * Created by grigoriykatz on 04/10/17.
 */

public class SearchBarFragment extends BaseMvpFragment implements SearchBarView
{
  public static final String TAG = SearchBarFragment.class.getName();

  private Drawable mIconClear;
  private Drawable mIconBackArrow;
  private Drawable mIconSearch;

  private boolean mIsFocused = false;

  @BindView(R.id.ddown_anchor) View ddown_anchor;
  @BindView(R.id.search_icon) ImageView search_icon;
  @BindView(R.id.supersearch_cancel) ImageView supersearch_cancel;
  @BindView(R.id.search_go) DelayAutoCompleteTextView search_go;
  @BindView(R.id.search_bar_search_progress) ProgressBar progressBar;
  @BindView(R.id.search_bar_background) FrameLayout search_bar_background;
  @BindView(R.id.search_cardview) CardView search_cardview;
  @BindView(R.id.ddown_anchor2) LinearLayout ddown_anchor2;

  SearchAutoCompleteAdapter2 searchAdapter;

  @InjectPresenter(type = PresenterType.LOCAL)
  SearchBarPresenter mPresenter;

  MainPresenter mParentPresenter;

  @ProvidePresenter(type = PresenterType.LOCAL)
  SearchBarPresenter provideSearchBarPresenter() {
    CustomPresenterFactory cpf = new CustomPresenterFactory();
    return cpf.provideSearchBarPresenter();
  }

  public static Fragment newInstance(final Context ctx, MainPresenter presenter)
  {
    SearchBarFragment fragment = new SearchBarFragment();
    fragment.mParentPresenter = presenter;
    return fragment;
  }

  public void setSearchActionIconColor(int color)
  {
    if (color == -1)
      color = CustomUtils.getColor(getContext(), R.color.def_action_icon);
    //mLeftActionIconColor = color;
    //mMenuBtnDrawable.setColor(color);
    DrawableCompat.setTint(mIconBackArrow, color);
    DrawableCompat.setTint(mIconSearch, color);
    DrawableCompat.setTint(mIconClear, color);

    search_icon.setImageDrawable(mIconSearch);
    //supersearch_cancel.setImageDrawable();
  }

  private void initDrawables()
  {
    mIconClear = CustomUtils.getWrappedDrawable(getContext(), R.drawable.ic_clear_black_24dp);
    mIconBackArrow = CustomUtils.getWrappedDrawable(getContext(), R.drawable.ic_arrow_back_black_24dp);
    mIconSearch = CustomUtils.getWrappedDrawable(getContext(), R.drawable.ic_search_black_24dp);
    setSearchActionIconColor(-1);
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_search_bar, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);

//    setupRecyclerView();
//    setupDataSync();
//    sharePresenter(mPresenter);

    //mParentPresenter.updateSearchTopicsView(this);
    initDrawables();
    search_icon.setOnClickListener(searchClickListener);
    supersearch_cancel.setOnClickListener(searchClickListener);


    searchAdapter = new SearchAutoCompleteAdapter2(getContext(), -1, mPresenter);
    search_go.setFocusable(true);
    search_go.setFocusableInTouchMode(true);
    search_go.setThreshold(3);
    search_go.setAdapter(searchAdapter);
    search_go.setLoadingIndicator(progressBar, search_icon);
    //search_go.setDropDownWidth(ddown_anchor2.getLayoutParams().width);

    search_go.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
      {
        Object object = (Object) adapterView.getItemAtPosition(position);
        if (object instanceof Suggestion)
        {
          search_go.setText(((Suggestion) object).getText());
          processSearchAction();
        }
        else if (object instanceof SearchAutoCompleteAdapter2.HeaderItem)
        {
          search_go.setText(((SearchAutoCompleteAdapter2.HeaderItem)object).getName().toLowerCase());
          processSearchAction();
        }
        else
        {

        }

      }
    });


    //TODO: disabled temporarily

    search_go.setOnFocusChangeListener(new TextView.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View v, boolean hasFocus) {

//        if (mSkipQueryFocusChangeEvent) {
//          mSkipQueryFocusChangeEvent = false;
//        } else
        if (hasFocus != mIsFocused) {
          setSearchFocusedInternal(hasFocus);
        }
      }
    });


    /**
     * handle of softKeyboard search button
     */
    search_go.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH)
        {
          processSearchAction();
          return true;
        }
        return false;
      }
    });





    //do not enable this string
    //search_go.setOnItemClickListener(mAutocompleteClickListener);

    search_go.addTextChangedListener(new TextWatcher()
    {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after)
      {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count)
      {

      }

      @Override
      public void afterTextChanged(Editable s)
      {

      }
    });

  }

  @Override
  public void onBackPressed()
  {

  }

  @Override
  public void onStop()
  {
    if (mPresenter != null)
      mPresenter.stop();
    super.onStop();
  }

  @Override
  public void onDestroyView()
  {
    if (mParentPresenter != null)
      mParentPresenter.updateSearchTopicsView(null);
    super.onDestroyView();
  }

  private void processSearchAction()
  {
    hideSearchWhenFocusLeftEditText();
    if (search_go != null)
      search_go.dismissDropDown();

    if (mParentPresenter != null)
    {
      String text = search_go.getText().toString();

      if (text != null && text.length() > 2)
      {
        mParentPresenter.res_startSearch(text);
      }
      else
      {
        //mParentPresenter.res_startSearch(null);
      }


    }
  }

  private void setSearchFocusedInternal(final boolean focused)
  {
    this.mIsFocused = focused;

    if (focused)
    {
      if (search_go != null)
        search_go.requestFocus();
      //fadeInBackground();
      if (mParentPresenter != null)
        mParentPresenter.showSearchBG();
      if (search_go != null && search_go.getText().toString().length() > 0 && supersearch_cancel != null)
        supersearch_cancel.setImageDrawable(mIconClear);
      if (search_go != null)
        CustomUtils.showSoftKeyboard(getContext(), search_go);
    }
    else
    {
      hideSearchWhenFocusLeftEditText();
    }
  }


  private void hideSearchWhenFocusLeftEditText()
  {
    hideKeyboard();
    //fadeOutBackground();
    if (mParentPresenter != null)
    {
      mParentPresenter.requestFocusFromEditText();
      mParentPresenter.hideSearchBG();
    }

    if (supersearch_cancel != null)
      supersearch_cancel.setImageDrawable(null);
    if (getActivity() != null)
      CustomUtils.closeSoftKeyboard(getActivity());
  }

  private void hideKeyboard()
  {
    InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
    if (search_go != null)
      imm.hideSoftInputFromWindow(search_go.getWindowToken(), 0);
    else
    {
      CustomUtils.closeSoftKeyboard(getActivity());
    }
  }


  public boolean isTextContainerOk()
  {
    return search_go != null && search_go.getText() != null && search_go.length() > 2;
  }

  public String getSearchContainerText()
  {
    if (isTextContainerOk())
      return search_go.getText().toString();
    else
      return "";
  }


  private View.OnClickListener searchClickListener = new View.OnClickListener()
  {
    @Override
    public void onClick(View v)
    {
      if (v.getId() == R.id.supersearch_cancel)
      {
        search_go.setText("");
        supersearch_cancel.setImageDrawable(null);
      }
      else if (v.getId() == R.id.search_icon)
      {
        //mParentPresenter.toggleViews("");

      }

    }
  };


  public void runSearchStringUpdate(int newScreenState)
  {
    Action0 act = new Action0()
    {
      @Override
      public void call()
      {
        ((SearchAutoCompleteAdapter2)search_go.getAdapter()).updateScreenState(newScreenState);
        search_go.clearComposingText();
      }
    };

    Schedulers.immediate().createWorker().schedule(act);

//    if (newScreenState == ComplexSearchMainPresenter.ScreenStates.ScreenSecond)
//    {
//      search_icon.setImageDrawable(mIconBackArrow);
//    }
//    else
    if (1==1) //(newScreenState == ComplexSearchMainPresenter.ScreenStates.ScreenCommon)
    {
      search_icon.setImageDrawable(mIconSearch);
      search_go.setText("");
    }

    if (search_go.getText() != null &&   search_go.getText().toString() != null
            && search_go.getText().toString().length() > 2)
    {
//
//      if (mPresenter != null)
//        mPresenter.findTopic(search_go.toString());
//      mParentPresenter.res_startSearch(search_go.getText().toString());
//

    }
  }


  boolean mHeaderIsShown = true;


  public void showReceived()
  {
    if (ddown_anchor == null)
      return;

    if (mHeaderIsShown)
      return;

    if (ddown_anchor.getAnimation() != null
            && ddown_anchor.getAnimation().isInitialized())
      return;
    //BaseView.cancelAnimation(head_layout);

    //int height = bottom_navigation_container.getLayoutParams().height;
    int height = 0;
    if (ddown_anchor instanceof LinearLayout)
    {
      height = ddown_anchor.getHeight();
    }
    else
    {
      height = ddown_anchor.getLayoutParams().height;
    }

    if ((ddown_anchor.getVisibility() == View.GONE || ddown_anchor.getVisibility() == View.INVISIBLE))
    {
      ddown_anchor.setTranslationY(-height);
      ddown_anchor.setVisibility(View.VISIBLE);

      ddown_anchor
              .animate()
              .translationY(0)
              .setDuration(300)
              .setInterpolator(new DecelerateInterpolator())
              .setListener(new Animator.AnimatorListener()
              {
                @Override
                public void onAnimationStart(Animator animation)
                {

                }

                @Override
                public void onAnimationEnd(Animator animation)
                {
                  /**
                   * should become visible before animation
                   */
                  //bottom_navigation.setVisibility(View.VISIBLE);

                }

                @Override
                public void onAnimationCancel(Animator animation)
                {
                  //bottom_navigation.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animator animation)
                {

                }
              }).start();
      mHeaderIsShown = true;
    }
  }


  public void hideReceived()
  {
    if (ddown_anchor == null)
      return;

    if (!mHeaderIsShown)
      return;

    if (ddown_anchor.getAnimation() != null
            && ddown_anchor.getAnimation().isInitialized())
      return;;
    //BaseView.cancelAnimation(head_layout);

    int height = 0;

    if (ddown_anchor instanceof LinearLayout)
    {
      height = ddown_anchor.getHeight();
    }
    else
    {
      height = ddown_anchor.getLayoutParams().height;
    }

    if (!(ddown_anchor.getVisibility() == View.GONE || ddown_anchor.getVisibility() == View.INVISIBLE))
    {
      ddown_anchor
              .animate()
              .translationY(-height)
              .setDuration(300)
              .setInterpolator(new AccelerateInterpolator())
              .setListener(new Animator.AnimatorListener()
              {
                @Override
                public void onAnimationStart(Animator animation)
                {

                }

                @Override
                public void onAnimationEnd(Animator animation)
                {
                  ddown_anchor.setVisibility(View.GONE);

                }

                @Override
                public void onAnimationCancel(Animator animation)
                {
                  ddown_anchor.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animator animation)
                {

                }
              }).start();
      mHeaderIsShown = false;
    }
  }

}
