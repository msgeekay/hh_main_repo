package com.msgeekay.hhtest.presentationlayer.di.common.components;

import android.content.Context;

import com.msgeekay.hhtest.datalayer.db.DBApi;
import com.msgeekay.hhtest.datalayer.image.ImageLoader;
import com.msgeekay.hhtest.datalayer.image.implv1.ImgCache;
import com.msgeekay.hhtest.datalayer.net.BaseApi;
import com.msgeekay.hhtest.domainlayer.executor.PostExecutionThread;
import com.msgeekay.hhtest.domainlayer.executor.ThreadExecutor;
import com.msgeekay.hhtest.presentationlayer.MyApp;
import com.msgeekay.hhtest.presentationlayer.Preferences;
import com.msgeekay.hhtest.presentationlayer.di.common.modules.ApplicationModule;
import com.msgeekay.hhtest.presentationlayer.di.common.modules.UseCasesModule;
import com.msgeekay.hhtest.presentationlayer.ui.common.CustomPresenterFactory;
import com.msgeekay.hhtest.presentationlayer.ui.common.mvp.BaseMvpActivity;
import com.msgeekay.hhtest.presentationlayer.ui.detail.DetailPresenter;
import com.msgeekay.hhtest.presentationlayer.ui.detail.detailscreen.DetailScreenPresenter;
import com.msgeekay.hhtest.presentationlayer.ui.main.list.CommonPresenter;
import com.msgeekay.hhtest.presentationlayer.ui.main.MainPresenter;
import com.msgeekay.hhtest.presentationlayer.ui.main.search.SearchBarPresenter;
import com.msgeekay.hhtest.presentationlayer.ui.splash.SplashPresenter;

import javax.inject.Singleton;
import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by grigoriykatz on 09/05/17.
 *
 * A component whose lifetime is the life of the application.
 */

@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = {ApplicationModule.class, UseCasesModule.class})
public interface ApplicationComponent
{
  void inject(BaseMvpActivity baseActivity);
  void inject(CustomPresenterFactory x);
  void inject(SplashPresenter x);
  void inject(MainPresenter x);
  void inject(CommonPresenter x);
  void inject(DetailPresenter x);
  void inject(DetailScreenPresenter x);
  void inject(SearchBarPresenter x);

  //Exposed to sub-graphs.
  MyApp app();
  Context context();
  ImgCache bitmapCache();
  ThreadExecutor threadExecutor();
  PostExecutionThread postExecutionThread();
  Preferences preferences();
  Retrofit retrofit();
  BaseApi api();
  ImageLoader imageLoader();
  DBApi dbApi();
}
