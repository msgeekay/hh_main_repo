package com.msgeekay.hhtest.presentationlayer.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by grigoriykatz on 11/05/17.
 */

public class CustomUtils
{
  private static int screenWidth = 0;
  private static int screenHeight = 0;

  public static int dpToPx(int dp) {
    return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
  }

  public static int getScreenHeight(Context c) {
    if (screenHeight == 0) {
      WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
      Display display = wm.getDefaultDisplay();
      Point size = new Point();
      display.getSize(size);
      screenHeight = size.y;
    }

    return screenHeight;
  }

  public static int getScreenWidth(Context c) {
    if (screenWidth == 0) {
      WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
      Display display = wm.getDefaultDisplay();
      Point size = new Point();
      display.getSize(size);
      screenWidth = size.x;
    }

    return screenWidth;
  }

  public static boolean isAndroid5() {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
  }

  public static String getCurrentProcessName(Context context) {
    String processName = "";
    int pid = android.os.Process.myPid();
    ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    for (ActivityManager.RunningAppProcessInfo processInfo : manager.getRunningAppProcesses()) {
      if (processInfo.pid == pid) {
        processName = processInfo.processName;
        break;
      }
    }
    return processName;
  }

  public static void showSoftKeyboard(final Context context, final EditText editText) {

    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {

        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_FORCED);
      }
    }, 100);
  }

  public static void closeSoftKeyboard(Activity activity) {

    View currentFocusView = activity.getCurrentFocus();
    if (currentFocusView != null) {
      InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(currentFocusView.getWindowToken(), 0);
    }
  }

  public static int getColor(Context context, @ColorRes int resId) throws Resources.NotFoundException {
    return ContextCompat.getColor(context, resId);
  }

  /**
   * Gets a reference to a given drawable and prepares it for use with tinting through.
   *
   * @param resId the resource id for the given drawable
   * @return a wrapped drawable ready fo use
   * with {@link android.support.v4.graphics.drawable.DrawableCompat}'s tinting methods
   * @throws Resources.NotFoundException
   */
  public static Drawable getWrappedDrawable(Context context, @DrawableRes int resId) throws Resources.NotFoundException {
    return DrawableCompat.wrap(ResourcesCompat.getDrawable(context.getResources(),
            resId, null));
  }
}
