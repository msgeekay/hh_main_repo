package com.msgeekay.hhtest.presentationlayer.ui.detail.detailscreen;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.msgeekay.hhtest.R;
import com.msgeekay.hhtest.domainlayer.exception.ErrorBundle;
import com.msgeekay.hhtest.model.Vacancy;
import com.msgeekay.hhtest.presentationlayer.ui.common.CustomPresenterFactory;
import com.msgeekay.hhtest.presentationlayer.ui.common.MasterScreen;
import com.msgeekay.hhtest.presentationlayer.ui.common.mvp.BaseMvpFragment;
import com.msgeekay.hhtest.presentationlayer.ui.main.list.CommonFragment;
import com.msgeekay.hhtest.presentationlayer.ui.main.list.CommonPresenter;

import java.lang.ref.WeakReference;

import butterknife.BindView;

/**
 * Created by grigoriykatz on 03/10/17.
 */

public class DetailFragment extends BaseMvpFragment implements DetailScreenView
{
  public static final String TAG = DetailFragment.class.getName();

  @BindView(R.id.imgHolder) ImageView image;
  @BindView(R.id.title) TextView title;
  @BindView(R.id.body) TextView body;

  @InjectPresenter(type = PresenterType.LOCAL)
  DetailScreenPresenter mPresenter;

  @ProvidePresenter(type = PresenterType.LOCAL)
  DetailScreenPresenter provideDetailScreenPresenter() {
    CustomPresenterFactory cpf = new CustomPresenterFactory();
    return cpf.provideDetailScreenPresenter();
  }

  public static Fragment newInstance(final Context ctx) {
    DetailFragment fragment = new DetailFragment();

    return fragment;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    Log.d(TAG, "onCreateView");
    return inflater.inflate(R.layout.fragment_detail, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    Log.d(TAG, "onViewCreated");
    sharePresenter(mPresenter);
//    setupRecyclerView();
//
//    setupDataSync();
  }

  @Override
  public void onStart()
  {
    super.onStart();
    Log.d(TAG, "onStart");
    if (mPresenter != null)
      sharePresenter(mPresenter);
  }

  @Override
  public void onStop()
  {
    Log.d(TAG, "onStop");
    if (mPresenter != null)
      mPresenter.stop();
    sharePresenter(null);
    super.onStop();
  }

  private void sharePresenter(MvpPresenter presenter)
  {
    Log.d(TAG, "sharePresenter");
    if (getActivity() != null && getActivity() instanceof MasterScreen)
    {
      ((MasterScreen)getActivity()).setDetailPresenter(presenter);
    }
  }

  @Override
  public void onBackPressed()
  {

  }

  @Override
  public void setVacancy(Vacancy vacancy)
  {
    title.setText(vacancy.getName());

    body.setText(Html.fromHtml(vacancy.getDescription()));
    if (mPresenter != null)
      mPresenter.loadImage(new WeakReference<ImageView>(image), vacancy.getImageURI());
  }

  @Override
  public void hideViewLoading()
  {

  }

  @Override
  public void showErrorMessage(ErrorBundle errorBundle)
  {

  }
}
