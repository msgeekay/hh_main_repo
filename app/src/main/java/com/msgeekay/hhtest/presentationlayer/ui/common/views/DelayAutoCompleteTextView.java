package com.msgeekay.hhtest.presentationlayer.ui.common.views;

import android.animation.Animator;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;

/**
 * Created by grigoriykatz on 26/05/17.
 */

public class DelayAutoCompleteTextView extends AppCompatAutoCompleteTextView
{

  private static final int MESSAGE_TEXT_CHANGED = 100;
  private static final int DEFAULT_AUTOCOMPLETE_DELAY = 750;

  private int mAutoCompleteDelay = DEFAULT_AUTOCOMPLETE_DELAY;
  private ProgressBar mLoadingIndicator;
  private View search_button;

  private final Handler mHandler = new Handler() {
    @Override
    public void handleMessage(Message msg) {
      DelayAutoCompleteTextView.super.performFiltering((CharSequence) msg.obj, msg.arg1);
    }
  };

  public DelayAutoCompleteTextView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public void setLoadingIndicator(ProgressBar progressBar, View search_button) {
    this.mLoadingIndicator = progressBar;
    this.search_button = search_button;
  }

  public void setAutoCompleteDelay(int autoCompleteDelay) {
    mAutoCompleteDelay = autoCompleteDelay;
  }

  @Override
  protected void performFiltering(CharSequence text, int keyCode) {
    if (mLoadingIndicator != null && search_button != null) {
      mLoadingIndicator.animate().alpha(1).setDuration(100);
      search_button.animate().alpha(0).setDuration(100).setListener(new Animator.AnimatorListener()
      {
        @Override
        public void onAnimationStart(Animator animation)
        {
          mLoadingIndicator.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAnimationEnd(Animator animation)
        {
          mLoadingIndicator.setAlpha(1);
          search_button.setAlpha(0);
        }

        @Override
        public void onAnimationCancel(Animator animation)
        {
          mLoadingIndicator.setAlpha(1);
          search_button.setAlpha(0);
        }

        @Override
        public void onAnimationRepeat(Animator animation)
        {

        }
      });
    }
    mHandler.removeMessages(MESSAGE_TEXT_CHANGED);
    mHandler.sendMessageDelayed(mHandler.obtainMessage(MESSAGE_TEXT_CHANGED, text), mAutoCompleteDelay);
  }

  @Override
  public void onFilterComplete(int count) {

    if (mLoadingIndicator != null && search_button != null) {
      mLoadingIndicator.animate().alpha(0).setDuration(100);
      search_button.animate().alpha(1).setDuration(100).setListener(new Animator.AnimatorListener()
      {
        @Override
        public void onAnimationStart(Animator animation)
        {
          mLoadingIndicator.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAnimationEnd(Animator animation)
        {
          mLoadingIndicator.setAlpha(0);
          search_button.setAlpha(1);
          DelayAutoCompleteTextView.super.onFilterComplete(count);
        }

        @Override
        public void onAnimationCancel(Animator animation)
        {
          mLoadingIndicator.setAlpha(0);
          search_button.setAlpha(1);
          DelayAutoCompleteTextView.super.onFilterComplete(count);
        }

        @Override
        public void onAnimationRepeat(Animator animation)
        {

        }
      });
    }



//    if (mLoadingIndicator != null) {
//      mLoadingIndicator.setVisibility(View.GONE);
//    }

  }
}
