package com.msgeekay.hhtest.presentationlayer.ui.main.list;

import android.widget.ImageView;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.fernandocejas.frodo.annotation.RxLogSubscriber;
import com.msgeekay.hhtest.datalayer.image.ImageLoader;
import com.msgeekay.hhtest.domainlayer.exception.DefaultErrorBundle;
import com.msgeekay.hhtest.domainlayer.interactor.DefaultSubscriber;
import com.msgeekay.hhtest.domainlayer.interactor.GetVacancyList;
import com.msgeekay.hhtest.model.Vacancy;
import com.msgeekay.hhtest.model.VacancyQuery;
import com.msgeekay.hhtest.presentationlayer.MyApp;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by grigoriykatz on 18/09/17.
 */

@InjectViewState
public class CommonPresenter extends MvpPresenter<CommonView>
{

  private List<Vacancy> vacancyList = new ArrayList<>();
  private GetVacancyList getVacancyList;
  private ImageLoader imageLoader;

  private VacancyQuery vacancyQuery = new VacancyQuery();

  @Inject
  public CommonPresenter(GetVacancyList getVacancyList, ImageLoader imageLoader)
  {
    this.getVacancyList = getVacancyList;
    this.imageLoader = imageLoader;

    MyApp.get().getApplicationComponent().inject(this);
  }


  public void stop()
  {
    getVacancyList.unsubscribe();
  }

  public void complexInit()
  {
    getVacancies(true);
  }

  public void complexInitVacanciesRefresh()
  {
    getVacancies(true);
  }

  public void getVacancies(boolean isRefresh) {
    if (isRefresh)
    {
      vacancyQuery = new VacancyQuery();
    }
    else
    {
      vacancyQuery.setPage(vacancyQuery.getPage() + 1);
    }
    getVacancyList.execute(vacancyQuery, new VacancyListSubscriber(isRefresh));
  }

  public void getVacanciesBySearch(String text, boolean isRefresh) {
    if (isRefresh)
    {
      vacancyQuery = new VacancyQuery();
      vacancyQuery.setText(text);
    }
    else
    {
      vacancyQuery.setPage(vacancyQuery.getPage() + 1);
    }
    getVacancyList.execute(vacancyQuery, new VacancyListSubscriber(isRefresh));
  }

  public List<Vacancy> getLastVacancyList()
  {
    return vacancyList;
  }

  public void loadImage(WeakReference<ImageView> viewWR, String url)
  {
    if (imageLoader != null)
      imageLoader.load(viewWR, url);
  }

  public void updateAdapterListener(VacanciesAdapterList.OnVacancyClickListener listener)
  {
    if (getViewState() != null)
      getViewState().updateListener(listener);
  }

  @RxLogSubscriber
  private final class VacancyListSubscriber extends DefaultSubscriber<List<Vacancy>>
  {
    private boolean isRefresh;

    public VacancyListSubscriber(boolean isRefresh) { this.isRefresh = isRefresh; }

    @Override public void onCompleted() {
      getViewState().hideViewLoading();

    }

    @Override public void onError(Throwable e) {
      getViewState().hideViewLoading();
      if (getViewState() != null)
        getViewState().showErrorMessage(new DefaultErrorBundle((Exception) e));
      ////getViewState().showViewRetry();
    }

    @Override public void onNext(List<Vacancy> vacancyList) {

      if (isRefresh)
      {
        CommonPresenter.this.vacancyList.clear();
      }
      CommonPresenter.this.vacancyList.addAll(vacancyList);
      getViewState().setVacancies(CommonPresenter.this.vacancyList);
    }
  }




}