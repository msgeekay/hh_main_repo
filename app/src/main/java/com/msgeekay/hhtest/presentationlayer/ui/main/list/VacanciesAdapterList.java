package com.msgeekay.hhtest.presentationlayer.ui.main.list;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.msgeekay.hhtest.R;
import com.msgeekay.hhtest.model.Vacancy;
import com.msgeekay.hhtest.presentationlayer.ui.common.anim.spring.springyRecyclerView.SpringyAdapterAnimationType;
import com.msgeekay.hhtest.presentationlayer.ui.common.anim.spring.springyRecyclerView.SpringyAdapterAnimator;
import com.msgeekay.hhtest.presentationlayer.ui.common.transitions.TransitionUtils;
import com.msgeekay.hhtest.presentationlayer.utils.CustomUtils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by grigoriykatz on 22/09/17.
 */

public class VacanciesAdapterList extends RecyclerView.Adapter<VacanciesAdapterList.VacancyViewHolder>
{

  public interface OnVacancyClickListener {
    void onVacancyClicked(View sharedView, String transitionName,
                          final int position, Vacancy vacancy);
  }

  private CommonPresenter mPresenter;
  private OnVacancyClickListener listener;
  private SpringyAdapterAnimator mAnimator;
  private Context context;
  private List<Vacancy> vacancyList = new ArrayList<>();
  private static List<Long> backList = new ArrayList<>();

  private static final int ANIMATED_ITEMS_COUNT = 4;
  private int lastAnimatedPosition = -1;
  private Context animContext;

  public VacanciesAdapterList(OnVacancyClickListener listener, Context context,
                              RecyclerView container)
  {
    this.listener = listener;
    this.context = context;
    mAnimator = new SpringyAdapterAnimator(container);
    mAnimator.setSpringAnimationType(SpringyAdapterAnimationType.SLIDE_FROM_BOTTOM);
    mAnimator.addConfig(85,15);
  }

  private void runEnterAnimation(View view, int position) {
    if (position >= ANIMATED_ITEMS_COUNT - 1) {
      return;
    }

    if (position > lastAnimatedPosition) {
      lastAnimatedPosition = position;
      view.setTranslationY(CustomUtils.getScreenHeight(animContext));
      //view.setTranslationX(CustomUtils.getScreenWidth(context));
      view.animate()
              .translationY(0)
              //.translationX(0)
              .setInterpolator(new DecelerateInterpolator(3.f))
              .setDuration(700)
              .start();
    }
  }

  @Override
  public VacancyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType)
  {
    animContext = parent.getContext();
    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rv_note_list, parent, false);
    mAnimator.onSpringItemCreate(itemView);
    return new VacancyViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(final VacancyViewHolder holder, final int position)
  {
    //runEnterAnimation(holder.itemView, position);

    Vacancy vac = vacancyList.get(position);
    holder.title.setText(vac.getName());
    holder.body.setText(vac.getDescription());
    if (vac.getImageURI() == null
            || (vac.getImageURI() != null && vac.getImageURI().length() == 0))
      holder.placePhoto.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.default_profile_background));
    else
    {
      if (mPresenter != null)
        mPresenter.loadImage(new WeakReference<ImageView>(holder.placePhoto), vac.getImageURI());

//              Picasso.with(context)
//              .load(new File(vac.getImageURI()))
//              .placeholder(R.drawable.default_profile_background)
//              .error(R.drawable.default_profile_background)
//              //.fit()
//              //.centerInside()
//              .into(holder.placePhoto);
    }
    holder.root.setOnClickListener(view ->
            {
              if (listener == null)
                return;
              listener.onVacancyClicked(holder.root, TransitionUtils.getRecyclerViewTransitionName(position),
                      position, vacancyList.get(position));
            }
                    );

    mAnimator.onSpringItemBind(holder.itemView, position);
  }

  @Override
  public int getItemCount() {
    return vacancyList == null ? 0 : vacancyList.size();
  }

  @Override
  public long getItemId(int position)
  {
    return position;
  }


  public void setVacancyList(List<Vacancy> vacancyList) {
    this.vacancyList = vacancyList;
    //notifyDataSetChanged();
//    for (int i = 0; i < noteList.size(); i++) {
//      notifyItemInserted(i);
//    }
  }

  public void updateItemClickListener(OnVacancyClickListener listener)
  {
    this.listener = listener;
  }

  public void setPresenter(CommonPresenter presenter)
  {
    this.mPresenter = presenter;
  }


  static class VacancyViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.title) TextView title;
    @BindView(R.id.body) TextView body;
    @BindView(R.id.root) View root;
    @BindView(R.id.headerImage) CircleImageView placePhoto;

    VacancyViewHolder(final View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
