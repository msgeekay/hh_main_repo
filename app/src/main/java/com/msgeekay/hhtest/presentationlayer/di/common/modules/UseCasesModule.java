package com.msgeekay.hhtest.presentationlayer.di.common.modules;

import com.msgeekay.hhtest.domainlayer.executor.PostExecutionThread;
import com.msgeekay.hhtest.domainlayer.executor.ThreadExecutor;
import com.msgeekay.hhtest.domainlayer.interactor.GetSuggestionList;
import com.msgeekay.hhtest.domainlayer.interactor.GetVacancy;
import com.msgeekay.hhtest.domainlayer.interactor.GetVacancyList;
import com.msgeekay.hhtest.domainlayer.interactor.UseCase;
import com.msgeekay.hhtest.domainlayer.repo.Repository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by grigoriykatz on 18/05/17.
 */

@Module
public class UseCasesModule
{
  @Provides
  UseCase provideGetVacancyListUseCase(Repository r, ThreadExecutor threadExecutor,
                                     PostExecutionThread postExecutionThread) {
    return new GetVacancyList(r, threadExecutor, postExecutionThread);
  }

  @Provides
  GetVacancy provideGetVacancyUseCase(Repository r, ThreadExecutor threadExecutor,
                                      PostExecutionThread postExecutionThread) {
    return new GetVacancy(r, threadExecutor, postExecutionThread);
  }

  @Provides
  GetSuggestionList provideGetSuggestionListUseCase(Repository r, ThreadExecutor threadExecutor,
                                      PostExecutionThread postExecutionThread) {
    return new GetSuggestionList(r, threadExecutor, postExecutionThread);
  }

}
