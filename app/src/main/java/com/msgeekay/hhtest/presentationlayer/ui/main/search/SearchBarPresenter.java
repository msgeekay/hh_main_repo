package com.msgeekay.hhtest.presentationlayer.ui.main.search;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.msgeekay.hhtest.datalayer.image.ImageLoader;
import com.msgeekay.hhtest.domainlayer.interactor.GetSuggestionList;
import com.msgeekay.hhtest.model.Suggestion;
import com.msgeekay.hhtest.model.Vacancy;
import com.msgeekay.hhtest.presentationlayer.MyApp;
import com.msgeekay.hhtest.presentationlayer.utils.Parameters;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Created by grigoriykatz on 04/10/17.
 */

@InjectViewState
public class SearchBarPresenter extends MvpPresenter<SearchBarView>
{
  private List<Suggestion> suggestionList = new ArrayList<>();
  private GetSuggestionList getSuggestionList;

  //private VacancyQuery vacancyQuery = new VacancyQuery();

  @Inject
  public SearchBarPresenter(GetSuggestionList getSuggestionList)
  {
    this.getSuggestionList = getSuggestionList;

    MyApp.get().getApplicationComponent().inject(this);
  }

  public void stop()
  {
    getSuggestionList.unsubscribe();
  }

//  public List<Object> findTopicsSync(String text)
//  {
//    List<Object> retVal = new ArrayList<>();
//
//    mSearchQuery.setOffset(0);
//    mSearchQuery.setQ(text);
//    try
//    {
//      Future<Response<SearchTopicsModel>> future = mSearchTopics.getObservable(mSearchQuery).toBlocking().toFuture();
//      Response<SearchTopicsModel> resp = future.get();
//      if (resp.isSuccessful())
//      {
//        SearchTopicsModel model = resp.body();
//        if (model != null && model.getTotal() > 0)
//        {
//          List<Topic> lst = model.getItems();
//          if (lst != null)
//          {
//            retVal = new ArrayList<Object> (lst);
//          }
//        }
//      }
//    }
//    catch (Exception ex)
//    {
//      Log.d(getClass().getName(), ex.toString());
//    }
//
//    return retVal;
//  }

  public List<Object> findSuggestionsSync(String text)
  {
    List<Object> retVal = new ArrayList<>();

    try
    {
      Future<List<Suggestion>> future = getSuggestionList.getObservable(text).toBlocking().toFuture();
      List<Suggestion> lst = future.get();
      if (lst != null)
        retVal = new ArrayList<Object>(lst);

    }
    catch (Exception ex)
    {
      if (Parameters.DEBUG)
        ex.printStackTrace();
    }

    return retVal;
  }
}

