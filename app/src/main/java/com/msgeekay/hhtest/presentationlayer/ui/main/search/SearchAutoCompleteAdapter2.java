package com.msgeekay.hhtest.presentationlayer.ui.main.search;

import android.content.Context;
import android.support.annotation.WorkerThread;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.msgeekay.hhtest.R;
import com.msgeekay.hhtest.model.Suggestion;
import com.msgeekay.hhtest.presentationlayer.MyApp;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by grigoriykatz on 26/05/17.
 */

public class SearchAutoCompleteAdapter2 extends BaseAdapter implements Filterable
{

  //private int currentScreenState = ComplexSearchMainPresenter.ScreenStates.ScreenCommon;
  private static final int MAX_RESULTS = 10;
  private Context mContext;
  private SearchBarPresenter mPresenter;
  private List<Object> resultList = new ArrayList<Object>();

  public SearchAutoCompleteAdapter2(Context context, int screenState, SearchBarPresenter presenter) {
    mContext = context;
    //currentScreenState = screenState;
    mPresenter = presenter;
  }

  @WorkerThread
  public synchronized void updateScreenState(int newScreenState)
  {
    //this.currentScreenState = newScreenState;
    //if (newScreenState == ComplexSearchMainPresenter.ScreenStates.ScreenSecond)
      resultList = new ArrayList<>();
  }

  @Override
  public int getCount() {
    return resultList.size();
  }

  @Override
  public Object getItem(int index) {
    return resultList.get(index);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent)
  {
    Object o = resultList.get(position);

    if (convertView == null || 1==1)
    {
      LayoutInflater inflater = (LayoutInflater) mContext
              .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


      //convertView = inflater.inflate(R.layout.simple_dropdown_item_2line, parent, false);
      if (o instanceof HeaderItem)
        convertView = inflater.inflate(R.layout.custom_search_header, parent, false);
      else
        convertView = inflater.inflate(R.layout.custom_search_line_item, parent, false);
    }

    if (o instanceof HeaderItem)
    {
      HeaderItem h = ((HeaderItem) o);
      //convertView = inflater.inflate(R.layout.custom_search_header, parent, false);
      TextView name = (TextView) convertView.findViewById(R.id.textId);
      name.setText(h.getName() == null ? "" : h.getName());
    }
    else if (o instanceof Suggestion)
    {
      Suggestion t = (Suggestion)o;
      TextView title = (TextView) convertView.findViewById(R.id.text_1);
      TextView name1 = (TextView) convertView.findViewById(R.id.text_2);
      TextView name2 = (TextView) convertView.findViewById(R.id.text_3);

      title.setText(t.getText());

      name1.setText("");
      name2.setText("");
    }
    return convertView;
  }

  @Override
  public Filter getFilter() {
    Filter filter = new Filter() {
      private CharSequence lastConstraint;

//      final class DataSubscriber extends ResponseSubscriber<Response<SearchTopicsModel>, SearchTopicsModel>
//      {
//        @Override
//        public void doNext(SearchTopicsModel searchTopicsModel)
//        {
//          if (searchTopicsModel != null && searchTopicsModel.getItems() != null)
//          {
//            List<Object> books = new ArrayList<Object>();
//            HeaderItem h1 = new HeaderItem(App.getInstance()
//                    .getApplicationContext()
//                    .getString(R.string.nav_topics_title));
//            books.add(h1);
//            for (Topic t : searchTopicsModel.getItems())
//            {
//              books.add(t);
//            }
//
//            FilterResults filterResults = new FilterResults();
//            filterResults.values = books;
//            filterResults.count = books.size();
//            publishResults(lastConstraint, filterResults);
//          }
//        }
//      }

//      //@Override
//      protected FilterResults performFiltering_old(CharSequence constraint) {
//        FilterResults filterResults = new FilterResults();
//        if (constraint != null
//                && currentScreenState != ComplexSearchMainPresenter.ScreenStates.ScreenSecond)
//        {
//          lastConstraint = constraint;
//          if (mPresenter != null)
//            mPresenter.findTopic(constraint.toString(), new DataSubscriber());
//
//          return null;
//
////          List<Object> books = getDataFromApi(mContext, constraint.toString());
////
////          // Assign the data to the FilterResults
////          filterResults.values = books;
////          filterResults.count = books.size();
//        }
//        return filterResults;
//      }

      @Override
      protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults filterResults = new FilterResults();
        if (constraint != null)
        {
          lastConstraint = constraint;
          if (mPresenter != null)
          {
            List<Object> books = mPresenter.findSuggestionsSync(constraint.toString());
            if (books != null && books.size() > 0)
            {
              HeaderItem h1 = new HeaderItem(MyApp.getInstance()
                      .getApplicationContext()
                      .getString(R.string.nav_topics_title));
              books.add(0, h1);
            }

//          List<Object> books = getDataFromApi(mContext, constraint.toString());

            // Assign the data to the FilterResults
            filterResults.values = books;
            filterResults.count = books == null ? 0 : books.size();
          }
        }
        return filterResults;
      }

      @Override
      protected void publishResults(CharSequence constraint, FilterResults results) {
        if (results != null && results.count > 0) {
          Log.d(getClass().getName(), "res!=0 && res.cnt >0");
          resultList = (List<Object>) results.values;
          notifyDataSetChanged();
        } else  {
          Log.d(getClass().getName(), "else res!=0 ");
          notifyDataSetInvalidated();
        }
//        else {
//          Log.d(getClass().getName(), " else do nothing");
//          //do nothing
//        }

      }};
    return filter;
  }

  /**
   * Returns a search result for the given book title.
   */
//  private List<Object> findBooks(Context context, String bookTitle) {
//    // GoogleBooksProtocol is a wrapper for the Google Books API
//    GoogleBooksProtocol protocol = new GoogleBooksProtocol(context, MAX_RESULTS);
//    return protocol.findBooks(bookTitle);
//  }

//  private List<Object> getDataFromApi(Context context, String constraint)
//  {
//    List<Object> arr = new ArrayList<>();
//    HeaderItem h1 = new HeaderItem("This is header");
//    Topic t1 = new Topic();
//    t1.setTitle("This is Topic 1");
//    t1.setTotalQuestions(40);
//    Topic t2 = new Topic();
//    t2.setTitle("This is Topic 2");
//    t2.setTotalQuestions(30);
//    arr.add(h1); arr.add(t1); arr.add(t2);
//    return arr;
//  }

  public class HeaderItem
  {
    String name;

    public HeaderItem(String name)
    {
      this.name = name;
    }

    public String getName()
    {
      return name;
    }
  }
}
