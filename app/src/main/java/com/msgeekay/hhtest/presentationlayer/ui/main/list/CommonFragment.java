package com.msgeekay.hhtest.presentationlayer.ui.main.list;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.MvpPresenter;
import com.msgeekay.hhtest.model.Vacancy;
import com.msgeekay.hhtest.presentationlayer.ui.common.MasterScreen;
import com.msgeekay.hhtest.presentationlayer.ui.common.OnPaginationScrollListener;
import com.msgeekay.hhtest.presentationlayer.ui.common.views.StartOffsetItemDecoration;
import com.msgeekay.hhtest.presentationlayer.utils.CustomUtils;
//import com.transitionseverywhere.Scene;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.msgeekay.hhtest.R;
import com.msgeekay.hhtest.domainlayer.exception.ErrorBundle;
import com.msgeekay.hhtest.presentationlayer.MyApp;
import com.msgeekay.hhtest.presentationlayer.ui.common.CustomPresenterFactory;
import com.msgeekay.hhtest.presentationlayer.ui.common.mvp.BaseMvpActivity;
import com.msgeekay.hhtest.presentationlayer.ui.common.mvp.BaseMvpFragment;
import com.msgeekay.hhtest.presentationlayer.ui.main.MainActivity;

import java.util.List;

import butterknife.BindView;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public class CommonFragment extends BaseMvpFragment implements CommonView
{
  public static final String TAG = CommonFragment.class.getName();


  @BindView(R.id.common_swipe_refresh) SwipeRefreshLayout refresher;
  @BindView(R.id.recyclerviewList) RecyclerView recyclerViewList;

  @InjectPresenter(type = PresenterType.LOCAL)
  CommonPresenter mPresenter;

  @ProvidePresenter(type = PresenterType.LOCAL)
  CommonPresenter provideCommonPresenter() {
    CustomPresenterFactory cpf = new CustomPresenterFactory();
    return cpf.provideCommonPresenter();
  }

  public class ViewVersion
  {
    public static final int viewVertical = 0;
    public static final int viewHorizontalWMap = 1;
  }

  private List<Vacancy> vacanciesList;
  private VacanciesAdapterList vacanciesAdapterList;
  private OnPaginationScrollListener mPaginationScrollListener;
  private String currentTransitionName;
  private VacanciesAdapterList.OnVacancyClickListener vacancyClickListener;

  public static Fragment newInstance(final Context ctx) {
    CommonFragment fragment = new CommonFragment();

    return fragment;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_common, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);

    setupRecyclerView();

    setupDataSync();
    sharePresenter(mPresenter);

    refresher.setOnRefreshListener(() -> onRefresh());
    refresher.setProgressViewOffset(false, 0, CustomUtils.dpToPx(100));

  }

  @Override
  public void onStart()
  {
    super.onStart();
    if (mPresenter != null)
      sharePresenter(mPresenter);
  }

  @Override
  public void onStop()
  {
    if (mPresenter != null)
      mPresenter.stop();
    sharePresenter(null);
    super.onStop();
  }

  private void onRefresh()
  {
    if (mPresenter != null)
      mPresenter.getVacancies(true);
  }

  private void sharePresenter(MvpPresenter presenter)
  {
    if (getActivity() != null && getActivity() instanceof MasterScreen)
    {
      ((MasterScreen)getActivity()).setListPresenter(presenter);
    }

    if (vacanciesAdapterList != null)
      vacanciesAdapterList.setPresenter((CommonPresenter) presenter);
  }

  /** Step 1 **/
  private void setupRecyclerView()
  {
    recyclerViewList.setLayoutManager(new LinearLayoutManager(getContext(),
            LinearLayoutManager.VERTICAL,
            false));
    StartOffsetItemDecoration itemDecoration = new StartOffsetItemDecoration(CustomUtils.dpToPx(60));
    recyclerViewList.addItemDecoration(itemDecoration);
    vacanciesAdapterList = new VacanciesAdapterList(vacancyClickListener, getActivity(), recyclerViewList);

    mPaginationScrollListener = new OnPaginationScrollListener(
            (LinearLayoutManager) recyclerViewList.getLayoutManager()) {
      @Override
      public void load()
      {
        if (mPresenter != null)
          mPresenter.getVacancies(false);
      }
    };
    recyclerViewList.addOnScrollListener(mPaginationScrollListener);

  }

  private void setupData()
  {
    if (mPresenter != null)
      mPresenter.getVacancies(true);
  }

  public void refreshData()
  {
    if (mPresenter != null)
      mPresenter.complexInitVacanciesRefresh();
      //mPresenter.getNotes();
  }

  private void setupDataSync()
  {

    if (mPresenter != null)
      mPresenter.complexInit();
  }

  /** Step 2 **/
  @Override
  public void setVacancies(List<Vacancy> vacancyList)
  {
    recyclerViewList.setAdapter(vacanciesAdapterList);
    vacanciesAdapterList.setVacancyList(vacancyList);

    recyclerViewList.setHasFixedSize(true);
    recyclerViewList.setItemViewCacheSize(20);
    recyclerViewList.setDrawingCacheEnabled(true);
    recyclerViewList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

    notifyAdapters();
  }

  @Override
  public void updateListener(VacanciesAdapterList.OnVacancyClickListener listener)
  {
    this.vacancyClickListener = listener;
    this.vacanciesAdapterList.updateItemClickListener(listener);
  }

  private void notifyAdapters()
  {
    if (vacanciesAdapterList != null)
      vacanciesAdapterList.notifyDataSetChanged();
  }

  @Override
  public void hideViewLoading()
  {
    if (refresher != null && refresher.isRefreshing())
      refresher.setRefreshing(false);
    if (mPaginationScrollListener != null)
      mPaginationScrollListener.loadingComplete();
  }

  @Override
  public void showErrorMessage(ErrorBundle errorBundle)
  {
    Toast.makeText(MyApp.getInstance(), errorBundle.getErrorMessage(), Toast.LENGTH_SHORT).show();
  }


  @Override
  public void onBackPressed()
  {
      ((MainActivity) getActivity()).superOnBackPressed();
  }


//  public void swapViews()
//  {
//    if (isSwiping)
//      return;
//
//    isSwiping = true;
//    if (currentView == ViewVersion.viewVertical)
//    {
//      containerLayoutList.animate()
//                          .setDuration(300)
//                          .alpha(0)
//                          .setInterpolator(new AccelerateInterpolator())
//                          .setListener(new AnimatorListenerAdapter()
//                          {
//                            @Override
//                            public void onAnimationEnd(Animator animation)
//                            {
//                              containerLayoutList.setVisibility(View.GONE);
//                              isSwiping = false;
//                            }
//                          })
//                          .start();
//    }
//    else
//    {
//      containerLayoutList.animate()
//                          .setDuration(300)
//                           .alpha(1)
//                           .setInterpolator(new AccelerateInterpolator())
//                           .setListener(new AnimatorListenerAdapter()
//                           {
//                             @Override
//                             public void onAnimationEnd(Animator animation)
//                             {
//                               isSwiping = false;
//                             }
//
//                             @Override
//                             public void onAnimationStart(Animator animation)
//                             {
//                               containerLayoutList.setVisibility(View.VISIBLE);
//                             }
//                           })
//                           .start();
//    }
//  }
}
