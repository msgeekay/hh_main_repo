package com.msgeekay.hhtest.presentationlayer.ui.detail.detailscreen;

import android.widget.ImageView;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.fernandocejas.frodo.annotation.RxLogSubscriber;
import com.msgeekay.hhtest.datalayer.image.ImageLoader;
import com.msgeekay.hhtest.domainlayer.exception.DefaultErrorBundle;
import com.msgeekay.hhtest.domainlayer.interactor.DefaultSubscriber;
import com.msgeekay.hhtest.domainlayer.interactor.GetVacancy;
import com.msgeekay.hhtest.model.Vacancy;
import com.msgeekay.hhtest.presentationlayer.MyApp;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

/**
 * Created by grigoriykatz on 03/10/17.
 */

@InjectViewState
public class DetailScreenPresenter extends MvpPresenter<DetailScreenView>
{

  private GetVacancy getVacancy;
  private ImageLoader imageLoader;
  private Vacancy currentVacancy;

  @Inject
  public DetailScreenPresenter(GetVacancy getVacancy, ImageLoader imageLoader)
  {
    this.getVacancy = getVacancy;
    this.imageLoader = imageLoader;

    MyApp.get().getApplicationComponent().inject(this);
  }

  public void stop()
  {
    if (getVacancy != null)
      getVacancy.unsubscribe();
  }

  public void updateDetailScreen(long vacancyId)
  {
    if (vacancyId > 0)
    {
      getVacancy.execute(Long.valueOf(vacancyId), new DetailedVacancySubscriber());
    }
  }

  public void loadImage(WeakReference<ImageView> viewWR, String url)
  {
    if (imageLoader != null)
      imageLoader.load(viewWR, url);
  }

  @RxLogSubscriber
  private final class DetailedVacancySubscriber extends DefaultSubscriber<Vacancy>
  {

    @Override public void onCompleted() {
      getViewState().hideViewLoading();

    }

    @Override public void onError(Throwable e) {
      getViewState().hideViewLoading();
      if (getViewState() != null)
        getViewState().showErrorMessage(new DefaultErrorBundle((Exception) e));
      ////getViewState().showViewRetry();
    }

    @Override public void onNext(Vacancy vacancy) {

      DetailScreenPresenter.this.currentVacancy = vacancy;
      getViewState().setVacancy(vacancy);
    }
  }


}
