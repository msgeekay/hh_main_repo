package com.msgeekay.hhtest.presentationlayer.ui.main;

import android.content.Context;
import android.view.View;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.msgeekay.hhtest.model.Vacancy;
import com.msgeekay.hhtest.presentationlayer.MyApp;
import com.msgeekay.hhtest.presentationlayer.navigation.Navigator;
import com.msgeekay.hhtest.presentationlayer.ui.detail.DetailPresenter;
import com.msgeekay.hhtest.presentationlayer.ui.detail.detailscreen.DetailScreenPresenter;
import com.msgeekay.hhtest.presentationlayer.ui.main.list.CommonFragment;
import com.msgeekay.hhtest.presentationlayer.ui.main.list.CommonPresenter;
import com.msgeekay.hhtest.presentationlayer.ui.main.list.VacanciesAdapterList;
import com.msgeekay.hhtest.presentationlayer.ui.main.search.SearchBarView;

import javax.inject.Inject;

/**
 * Created by grigoriykatz on 17/09/17.
 */

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView>
{
  private CommonPresenter listPresenter;
  private DetailScreenPresenter detailPresenter;
  private Navigator navigator;
  private int viewVersion;
  private Context context;

  private VacanciesAdapterList.OnVacancyClickListener vacancyClickListener =
          new VacanciesAdapterList.OnVacancyClickListener()
  {
    @Override
    public void onVacancyClicked(View sharedView, String transitionName, int position, Vacancy vacancy)
    {
      MainPresenter.this.vacancyClicked(vacancy);
    }
  };

  @Inject
  public MainPresenter(Navigator navigator)
  {
    MyApp.get().getApplicationComponent().inject(this);

    this.navigator = navigator;
  }

  public void start(Context activityContext)
  {
    this.context = activityContext;
    getViewState().setSoftKeyboardToOverlapBottomScreenParts();
  }

  public void stop()
  {
    getViewState().setSoftKeyboardToOverlapBottomScreenPartsNOT();
  }

  public void setViewVersion(int viewVersion)
  {
    this.viewVersion = viewVersion;
  }

  public int getViewVersion()
  {
    return viewVersion;
  }

  public void setListPresenter(MvpPresenter listPresenter)
  {
    this.listPresenter = (CommonPresenter)listPresenter;
    if (this.listPresenter != null &&  getViewState() != null)
      ((CommonPresenter) listPresenter).updateAdapterListener(vacancyClickListener);
    else if (this.listPresenter != null)
      ((CommonPresenter) listPresenter).updateAdapterListener(null);
  }

  public void setDetailPresenter(MvpPresenter detailPresenter)
  {
    this.detailPresenter = (DetailScreenPresenter)detailPresenter;
  }

  public void vacancyClicked(Vacancy vacancy)
  {
    if (viewVersion == MainActivity.ViewVersion.VersionDetailed)
    {
      if (detailPresenter == null)
        return;
      detailPresenter.updateDetailScreen(vacancy.getId());
    }
    else if (viewVersion == MainActivity.ViewVersion.VersionShort)
    {
      navigator.navigateToDetails(context, vacancy.getId());
    }
  }


  /**
   * Extra child screen view's methods
   */

  public SearchBarView searchBarView;


  /****** Search result methods ***/
//  public void updateSearchMainResultView(ComplexSearchResultView view)
//  {
//    this.complexSearchResultView = view;
//  }
//
  public void res_startSearch(String text)
  {
    if (listPresenter == null)
      return;
    else
      listPresenter.getVacanciesBySearch(text, true);
  }




  /****** Search Bar methods ***/
  public void updateSearchTopicsView(SearchBarView view)
  {
    this.searchBarView = view;
  }

//  public boolean bar_isTextContainerOk()
//  {
//    if (searchBarView == null)
//      return false;
//    return searchBarView.isTextContainerOk();
//  }
//
//  public String bar_getSearchContainerText()
//  {
//    if (searchBarView == null)
//      return "";
//    return searchBarView.getSearchContainerText();
//
//  }

  /*** Main Screen methods ***/
  public void showSearchBG()
  {
    if (getViewState() != null)
      getViewState().showSearchBG();
  }

  public void hideSearchBG()
  {
    if (getViewState() != null)
      getViewState().hideSearchBG();
  }

  public void requestFocusFromEditText()
  {
    if (getViewState() != null)
      getViewState().requestFocusFromEditText();
  }
}
