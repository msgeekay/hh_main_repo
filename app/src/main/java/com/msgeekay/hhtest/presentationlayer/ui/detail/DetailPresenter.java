package com.msgeekay.hhtest.presentationlayer.ui.detail;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.msgeekay.hhtest.presentationlayer.MyApp;

import javax.inject.Inject;

/**
 * Created by grigoriykatz on 03/10/17.
 */

@InjectViewState
public class DetailPresenter extends MvpPresenter<DetailView>
{
  @Inject
  public DetailPresenter()
  {

    MyApp.get().getApplicationComponent().inject(this);
  }

  public void stop()
  {
  }
}
