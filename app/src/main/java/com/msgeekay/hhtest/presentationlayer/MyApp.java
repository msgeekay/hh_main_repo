package com.msgeekay.hhtest.presentationlayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.facebook.stetho.Stetho;
import com.google.gson.Gson;
import com.msgeekay.hhtest.BuildConfig;
import com.msgeekay.hhtest.R;
import com.msgeekay.hhtest.model.AccountAuth;
import com.msgeekay.hhtest.presentationlayer.di.common.components.ApplicationComponent;
import com.msgeekay.hhtest.presentationlayer.di.common.components.DaggerApplicationComponent;
import com.msgeekay.hhtest.presentationlayer.di.common.modules.ApplicationModule;
import com.msgeekay.hhtest.presentationlayer.ui.auth.LoginActivity;
import com.msgeekay.hhtest.presentationlayer.utils.CustomUtils;
import com.msgeekay.hhtest.presentationlayer.utils.Parameters;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.squareup.leakcanary.LeakCanary;

import java.util.regex.Pattern;

import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by grigoriykatz on 09/05/17.
 */

public class MyApp extends MultiDexApplication
{
  private static final String AUTH_KEY = "com.msgeekay.hhtest.shared.auth";

  private static MyApp sInstance;
  private ApplicationComponent applicationComponent;
  private String appProcessName = null;
  private static AccountAuth mAccountAuth;

  @Override public void onCreate() {
    super.onCreate();

    sInstance = this;
    this.initDatabase();
    this.initializeInjector();
    this.initializeLeakDetection();
    this.initCalligraphy();

    if (!BuildConfig.DEBUG) {
      Log.d("[TIMBER INIT]", "Timber.DebugTree()");
      Timber.plant(new CustomDebugTree());
    } else {
      Log.d("[TIMBER INIT]", "CrashReportingTree()");
      Timber.plant(new CrashReportingTree());
    }

    if (Parameters.DEBUG)
      Stetho.initializeWithDefaults(this);

  }

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(this);
  }

  private void initDatabase() {
    //FlowManager.init(new FlowConfig.Builder(this).build());
    FlowManager.init(this);
  }

  private void initializeInjector() {
    this.applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(new ApplicationModule(this))
            .build();

  }

  public ApplicationComponent getApplicationComponent() {
    return applicationComponent;
  }

  public static MyApp getContext(Context context) {
    return (MyApp) context.getApplicationContext();
  }

  public static MyApp getInstance()
  {
    return sInstance;
  }

  private void initializeLeakDetection() {
    if (BuildConfig.DEBUG) {
      LeakCanary.install(this);
    }
  }

  public static MyApp get()
  {
    return sInstance;
  }

  public synchronized String getAppProcessName()
  {
    if (appProcessName == null)
    {
      appProcessName = CustomUtils.getCurrentProcessName(this);
    }

    return appProcessName;
  }

  private void initCalligraphy() {
    CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
            .setDefaultFontPath("fonts/Roboto-Regular.ttf")
            .setFontAttrId(R.attr.fontPath)
            .build()
    );
  }

  /** A tree which logs important information for crash reporting. */
  private static class CrashReportingTree extends Timber.Tree
  {
    @Override protected void log(int priority, String tag, String message, Throwable t)
    {
      if (!BuildConfig.DEBUG)
      {
        if (priority == Log.VERBOSE || priority == Log.DEBUG)
        {
          return;
        }
      }

      FakeCrashLibrary.log(priority, tag, message);

      if (t != null)
      {
        if (priority == Log.ERROR)
        {
          FakeCrashLibrary.logError(tag, message, t);
        } else if (priority == Log.WARN)
        {
          FakeCrashLibrary.logWarning(tag, message, t);
        }
      }
    }

  }

  /** A {@link Timber.Tree Tree} for debug builds. Automatically infers the tag from the calling class. */
  public static class CustomDebugTree extends Timber.Tree
  {
    private static final int MAX_LOG_LENGTH = 4000;
    private static final int CALL_STACK_INDEX = 5;
    private static final Pattern ANONYMOUS_CLASS = Pattern.compile("(\\$\\d+)+$");



    /**
     * Break up {@code message} into maximum-length chunks (if needed) and send to either
     * {@link Log#println(int, String, String) Log.println()} or
     * {@link Log#wtf(String, String) Log.wtf()} for logging.
     *
     * {@inheritDoc}
     */
    @Override protected void log(int priority, String tag, String message, Throwable t) {
      if (message.length() < MAX_LOG_LENGTH) {
        if (priority == Log.ASSERT) {
          Log.wtf(tag, message);
        } else {
          Log.println(priority, tag, message);
        }
        return;
      }

      // Split by line, then ensure each line can fit into Log's maximum length.
      for (int i = 0, length = message.length(); i < length; i++) {
        int newline = message.indexOf('\n', i);
        newline = newline != -1 ? newline : length;
        do {
          int end = Math.min(newline, i + MAX_LOG_LENGTH);
          String part = message.substring(i, end);
          if (priority == Log.ASSERT) {
            Log.wtf(tag, part);
          } else {
            Log.println(priority, tag, part);
          }
          i = end;
        } while (i < newline);
      }
    }
  }


  public String getBaseUrl() {
    return getResources().getString(R.string.base_url);
  }

  /**
   * Получить авторизацию
   */
  public AccountAuth getAccountAuth() {
    return getAccountAuth(true);
  }

  /**
   * Получить авторизацию
   *
   * @param isUserAction флаг действия пользователя,
   *                     true - при отсутствии авторизации, показываем экран Авторизации
   */
  public AccountAuth getAccountAuth(boolean isUserAction) {
    if (mAccountAuth == null) {
      mAccountAuth = getSharedPrefAccountAuth();
      if (isUserAction) {
        startActivity(new Intent(this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
      }
    }
    return mAccountAuth;
  }

  public void setAccountAuth(AccountAuth accountAuth) {
    if (accountAuth != null) {
      saveSharedPrefAccountAuth(accountAuth);
    } else {
      removeSharedPrefAccountAuth();
    }
    mAccountAuth = accountAuth;
  }

  /**
   * Сохранить авторизацию в настройки
   */
  private void saveSharedPrefAccountAuth(AccountAuth accountAuth) {
    Gson gson = new Gson();
    String authString = gson.toJson(accountAuth);

    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
    SharedPreferences.Editor editor = sp.edit();
    editor.putString(AUTH_KEY, authString);
    editor.apply();
  }

  /**
   * Удалить авторизацию из настроек
   */
  private void removeSharedPrefAccountAuth() {
    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
    SharedPreferences.Editor editor = sp.edit();
    editor.remove(AUTH_KEY);
    editor.apply();
  }

  /**
   * Достать из настроек авторизацию
   */
  private AccountAuth getSharedPrefAccountAuth() {
    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
    String auth = sp.getString(AUTH_KEY, "");
    if (!"".equals(auth)) {
      Gson gson = new Gson();
      return gson.fromJson(auth, AccountAuth.class);
    }
    return null;
  }

  private void registerNetworkUpdate(BroadcastReceiver receiver)
  {
    final IntentFilter filter = new IntentFilter();
    filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
    getContext(this).registerReceiver(receiver, filter);
  }
}
