package com.msgeekay.hhtest.presentationlayer.di.common.modules;


import android.content.Context;

import com.msgeekay.hhtest.datalayer.db.DBApi;
import com.msgeekay.hhtest.datalayer.db.DBApiImpl;
import com.msgeekay.hhtest.datalayer.image.ImageLoader;
import com.msgeekay.hhtest.datalayer.image.implv2.ImageLoaderImpl;
import com.msgeekay.hhtest.datalayer.image.implv1.ImgCache;
import com.msgeekay.hhtest.datalayer.cache.DataCache;
import com.msgeekay.hhtest.datalayer.cache.DataCacheImpl;
import com.msgeekay.hhtest.datalayer.executor.JobExecutor;
import com.msgeekay.hhtest.datalayer.net.ApiBuilder;
import com.msgeekay.hhtest.datalayer.net.BaseApi;
import com.msgeekay.hhtest.datalayer.repo.DataRepository;
import com.msgeekay.hhtest.domainlayer.executor.PostExecutionThread;
import com.msgeekay.hhtest.domainlayer.executor.ThreadExecutor;
import com.msgeekay.hhtest.domainlayer.repo.Repository;
import com.msgeekay.hhtest.presentationlayer.MyApp;
import com.msgeekay.hhtest.presentationlayer.Preferences;
import com.msgeekay.hhtest.presentationlayer.UIThread;

import javax.inject.Named;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by grigoriykatz on 09/05/17.
 *
 * Dagger module that provides objects which will live during the application lifecycle.
 */
@Module
public class ApplicationModule {
  private final MyApp application;
  private final Preferences prefs;
  private final ApiBuilder apiBuilder;

  public ApplicationModule(MyApp application) {
    this.application = application;
    this.prefs = new Preferences(application);
    this.apiBuilder = new ApiBuilder();

  }

  @Provides
  @Singleton
  MyApp providesApp() {
    return application;
  }

  @Provides
  @Singleton
  Context provideApplicationContext() {
    return this.application.getApplicationContext();
  }

  @Provides
  @Singleton
  ImgCache provideBitmapCache() {
    return ImgCache.instance();
  }

  @Provides @Singleton
  ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
    return jobExecutor;
  }

  @Provides @Singleton
  PostExecutionThread providePostExecutionThread(UIThread uiThread) {
    return uiThread;
  }

  @Provides
  @Singleton
  Preferences providePreferences() {
    return prefs;
  }

  @Provides
  @Singleton
  ImageLoader providesImageLoader(ImageLoaderImpl imageLoader) {
    return imageLoader;
  }

  @Provides
  @Singleton
  Retrofit provideRetrofit(MyApp app) {
    return apiBuilder.getRetrofit(app.getBaseUrl());
  }

  @Provides
  @Singleton
  BaseApi provideApiInterface(Retrofit retrofit) {
    return apiBuilder.getApiInterface(retrofit);
  }

  @Provides
  @Singleton
  @Named("ProcName")
  String providesAppProcessName()
  {
    return application.getAppProcessName();
  }

  @Provides
  @Singleton
  Repository providesRepo(DataRepository dataRepository) { return dataRepository; }

  @Provides
  @Singleton
  DataCache provideDataCache(DataCacheImpl dataCache) {
    return dataCache;
  }

  @Provides
  @Singleton
  DBApi provideDBApi() {
    return new DBApiImpl();
  }



}

